@file:Suppress("DEPRECATION")

package com.behametrics.logger.inputloggers

import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.VariableArgEvent
import com.behametrics.logger.utils.maps.KeyboardKeycodeMap
import com.behametrics.logger.utils.session.SessionManager
import android.inputmethodservice.Keyboard

/**
 * Class providing logging of keystroke events (key press/release, key name, ...). Modifier keys
 * (e.g. shift) are logged as any regular character.
 *
 * @see VariableArgEvent
 */
class KeyboardLogger(
    inputEventDispatcher: InputEventDispatcher,
    sessionManager: SessionManager
) : VariableArgEventLogger(inputEventDispatcher, sessionManager, INPUT_NAME, CONTENT_FIELD_NAMES) {

    companion object {
        private var keyCodeMap: KeyboardKeycodeMap = KeyboardKeycodeMap()

        const val INPUT_NAME = "keyboard"

        @JvmField
        val CONTENT_FIELD_NAMES = arrayOf(
            "event_type",
            "key_number",
            "key_name"
        )
    }

    /**
     * Logs a keystroke event if logging is enabled (by calling [start]).
     *
     * @param eventType event type (usually `"press"` or `"release"`)
     * @param keyNumber number representing a Unicode character, or a special number representing a
     *   modifier key
     * @param keyName   name of the key pressed. If `null`, the [keyNumber] is translated to a
     *   Unicode character or is mapped to a modifier key (e.g. shift) based on the `KEYCODE_*`
     *   constants in the [Keyboard] class.
     */
    fun log(eventType: String, keyNumber: Int, keyName: String? = null) {
        super.log(
            eventType,
            keyNumber.toString(),
            keyName ?: keyCodeMap.getName(keyNumber) ?: keyNumber.toChar().toString())
    }
}
