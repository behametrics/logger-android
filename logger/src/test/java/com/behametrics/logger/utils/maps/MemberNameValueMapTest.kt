package com.behametrics.logger.utils.maps

import android.hardware.Sensor
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class MemberNameValueMapTest {

    private lateinit var map: MemberNameValueMap<Int>

    @BeforeEach
    fun setUp() {
        map = MemberNameValueMap(null, Class::class.java)

        map.put("accelerometer", Sensor.TYPE_ACCELEROMETER)
        map.put("gyroscope", Sensor.TYPE_GYROSCOPE)
    }

    @ParameterizedTest
    @MethodSource("getValueParams")
    fun getValue(name: String, expectedValue: Int?) {
        assertEquals(expectedValue, map.getValue(name))
    }

    @ParameterizedTest
    @MethodSource("containsNameParams")
    fun containsName(name: String, expectedContains: Boolean) {
        assertEquals(expectedContains, map.containsName(name))
    }

    @ParameterizedTest
    @MethodSource("getNameParams")
    fun getName(value: Int, expectedName: String?) {
        assertEquals(expectedName, map.getName(value))
    }

    @ParameterizedTest
    @MethodSource("containsValueParams")
    fun containsValue(value: Int, expectedContains: Boolean) {
        assertEquals(expectedContains, map.containsValue(value))
    }

    @Test
    fun put_NewName() {
        map.put("magnetic_field", Sensor.TYPE_MAGNETIC_FIELD)

        assertEquals(Sensor.TYPE_MAGNETIC_FIELD, map.getValue("magnetic_field"))
        assertEquals("magnetic_field", map.getName(Sensor.TYPE_MAGNETIC_FIELD))
    }

    @Test
    fun put_NewNameWithUppercaseCharacters() {
        map.put("MAGNETIC_FIELD", Sensor.TYPE_MAGNETIC_FIELD)

        assertEquals(Sensor.TYPE_MAGNETIC_FIELD, map.getValue("magnetic_field"))
        assertEquals("magnetic_field", map.getName(Sensor.TYPE_MAGNETIC_FIELD))
    }

    @Test
    fun put_ExistingName_OverridesValue() {
        map.put("accelerometer", Sensor.TYPE_MAGNETIC_FIELD)

        assertEquals(Sensor.TYPE_MAGNETIC_FIELD, map.getValue("accelerometer"))
        assertEquals("accelerometer", map.getName(Sensor.TYPE_MAGNETIC_FIELD))
    }

    companion object {
        @JvmStatic
        fun getValueParams() = arrayOf<Any?>(
            arrayOf<Any?>("accelerometer", Sensor.TYPE_ACCELEROMETER),
            arrayOf<Any?>("ACCELEROMETER", Sensor.TYPE_ACCELEROMETER),
            arrayOf<Any?>("Accelerometer", Sensor.TYPE_ACCELEROMETER),
            arrayOf<Any?>("type_accelerometer", null),
            arrayOf<Any?>("nonexistent_sensor", null),
            arrayOf<Any?>("", null)
        )

        @JvmStatic
        fun containsNameParams() = arrayOf<Any?>(
            arrayOf<Any?>("accelerometer", true),
            arrayOf<Any?>("ACCELEROMETER", true),
            arrayOf<Any?>("Accelerometer", true),
            arrayOf<Any?>("type_accelerometer", false),
            arrayOf<Any?>("nonexistent_sensor", false),
            arrayOf<Any?>("", false)
        )

        @JvmStatic
        fun getNameParams() = arrayOf<Any?>(
            arrayOf<Any?>(Sensor.TYPE_ACCELEROMETER, "accelerometer"),
            arrayOf<Any?>(-1, null)
        )

        @JvmStatic
        fun containsValueParams() = arrayOf<Any?>(
            arrayOf<Any?>(Sensor.TYPE_ACCELEROMETER, true),
            arrayOf<Any?>(-1, false)
        )
    }
}

internal class MemberNameValueMapTestWithInitializer {

    private lateinit var map: MemberNameValueMap<Int>

    @BeforeEach
    fun setUp() {
        map = MemberNameValueMap(PrefixBasedMapInitializer<Int>("TYPE_"), Sensor::class.java)
    }

    @Test
    fun getValue() {
        assertEquals(Sensor.TYPE_ACCELEROMETER, map.getValue("accelerometer"))
        assertEquals(Sensor.TYPE_GYROSCOPE, map.getValue("gyroscope"))
    }

    @Test
    fun getName() {
        assertEquals("accelerometer", map.getName(Sensor.TYPE_ACCELEROMETER))
        assertEquals("gyroscope", map.getName(Sensor.TYPE_GYROSCOPE))
    }
}
