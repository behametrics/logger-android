package com.behametrics.logger.callbacks

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.behametrics.logger.inputloggers.LifecycleLogger
import com.behametrics.logger.utils.logging.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity

/**
 * Class enabling automatic logging of touch data from [android.app.DialogFragment] or
 * [androidx.fragment.app.DialogFragment] instances.
 */
class FragmentLifecycleCallbackManager(private val lifecycleLogger: LifecycleLogger) {

    private val activitiesAndFragmentLifecycleTrackers = mutableMapOf<Activity, FragmentLifecycleTracker>()

    /**
     * Registers fragment lifecycle callbacks for the specified [activity].
     *
     * If the callbacks are already registered for the specified activity, this method has no
     * effect.
     *
     * This method returns true if the registration was successful and false otherwise (unsupported
     * callback registration due to low Android API version or the registration was already
     * performed for the [activity]).
     */
    fun registerCallbacks(activity: Activity): Boolean {
        if (activitiesAndFragmentLifecycleTrackers[activity] != null) {
            return false
        }

        var registrationSuccessful = true

        if (activity is FragmentActivity) {
            val fragmentLifecycleTracker = AndroidxFragmentLifecycleTracker(lifecycleLogger)

            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(fragmentLifecycleTracker, true)

            activitiesAndFragmentLifecycleTrackers[activity] = fragmentLifecycleTracker
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val fragmentLifecycleTracker = LegacyFragmentLifecycleTracker(lifecycleLogger)

                @Suppress("DEPRECATION")
                activity.fragmentManager.registerFragmentLifecycleCallbacks(fragmentLifecycleTracker, true)

                activitiesAndFragmentLifecycleTrackers[activity] = fragmentLifecycleTracker
            } else {
                registrationSuccessful = false
            }
        }

        return registrationSuccessful
    }

    /**
     * Unregister fragment lifecycle callbacks for the specified [activity].
     *
     * If the callbacks were not registered for the specified activity prior to calling this method, this method has no
     * effect.
     */
    fun unregisterCallbacks(activity: Activity) {
        val fragmentLifecycleTracker = activitiesAndFragmentLifecycleTrackers[activity] ?: return

        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                .unregisterFragmentLifecycleCallbacks(fragmentLifecycleTracker as AndroidxFragmentLifecycleTracker)
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                @Suppress("DEPRECATION")
                activity.fragmentManager
                    .unregisterFragmentLifecycleCallbacks(fragmentLifecycleTracker as LegacyFragmentLifecycleTracker)
            }
        }

        activitiesAndFragmentLifecycleTrackers.remove(activity)
    }
}

internal interface FragmentLifecycleTracker

internal class AndroidxFragmentLifecycleTracker(
    private val lifecycleLogger: LifecycleLogger
) : FragmentManager.FragmentLifecycleCallbacks(),
    FragmentLifecycleTracker {

    override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
        super.onFragmentResumed(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "resumed")

        if (f is DialogFragment) {
            f.startTouchLogging()
        }
    }

    override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
        super.onFragmentPaused(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "paused")

        if (f is DialogFragment) {
            f.stopTouchLogging()
        }
    }

    override fun onFragmentAttached(fm: FragmentManager, f: Fragment, context: Context) {
        super.onFragmentAttached(fm, f, context)

        lifecycleLogger.log("fragment", f.javaClass.name, "attached")
    }

    override fun onFragmentCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
        super.onFragmentCreated(fm, f, savedInstanceState)

        lifecycleLogger.log("fragment", f.javaClass.name, "created")
    }

    override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
        super.onFragmentStarted(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "started")
    }

    override fun onFragmentStopped(fm: FragmentManager, f: Fragment) {
        super.onFragmentStopped(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "stopped")
    }

    override fun onFragmentDestroyed(fm: FragmentManager, f: Fragment) {
        super.onFragmentDestroyed(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "destroyed")
    }

    override fun onFragmentDetached(fm: FragmentManager, f: Fragment) {
        super.onFragmentDetached(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "detached")
    }
}

@Suppress("DEPRECATION")
@RequiresApi(api = Build.VERSION_CODES.O)
internal class LegacyFragmentLifecycleTracker(
    private val lifecycleLogger: LifecycleLogger
) : android.app.FragmentManager.FragmentLifecycleCallbacks(),
    FragmentLifecycleTracker {

    override fun onFragmentResumed(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentResumed(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "resumed")

        if (f is android.app.DialogFragment) {
            f.startTouchLogging()
        }
    }

    override fun onFragmentPaused(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentPaused(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "paused")

        if (f is android.app.DialogFragment) {
            f.stopTouchLogging()
        }
    }

    override fun onFragmentAttached(fm: android.app.FragmentManager, f: android.app.Fragment, context: Context) {
        super.onFragmentAttached(fm, f, context)

        lifecycleLogger.log("fragment", f.javaClass.name, "attached")
    }

    override fun onFragmentCreated(
        fm: android.app.FragmentManager,
        f: android.app.Fragment,
        savedInstanceState: Bundle
    ) {
        super.onFragmentCreated(fm, f, savedInstanceState)

        lifecycleLogger.log("fragment", f.javaClass.name, "created")
    }

    override fun onFragmentStarted(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentStarted(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "started")
    }

    override fun onFragmentStopped(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentStopped(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "stopped")
    }

    override fun onFragmentDestroyed(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentDestroyed(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "destroyed")
    }

    override fun onFragmentDetached(fm: android.app.FragmentManager, f: android.app.Fragment) {
        super.onFragmentDetached(fm, f)

        lifecycleLogger.log("fragment", f.javaClass.name, "detached")
    }
}
