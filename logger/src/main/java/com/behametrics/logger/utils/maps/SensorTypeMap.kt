package com.behametrics.logger.utils.maps

import android.hardware.Sensor
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer

private const val MEMBER_NAME_PREFIX = "TYPE_"

/**
 * A map of sensor names to sensor types and vice versa according to [Sensor]. All static members of
 * the [Sensor] class prefixed with [MEMBER_NAME_PREFIX] are included in the map.
 *
 * Note that since the map is generated dynamically, some members of [Sensor] prefixed with
 * [MEMBER_NAME_PREFIX] are not actual sensors (such as [Sensor.TYPE_DEVICE_PRIVATE_BASE].
 */
class SensorTypeMap : MemberNameValueMap<Int>(
    PrefixBasedMapInitializer(MEMBER_NAME_PREFIX), Sensor::class.java)
