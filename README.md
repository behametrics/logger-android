# Behametrics - Android Logger

This is an Android library that allows you to easily log (collect) data in your Android application
from the touch screen, sensors (accelerometer, gyroscope, etc.) and several other inputs.
This library does not require root access to the device and logging is mostly limited to your application.

By default, the logged data are stored as CSV files for each input separately
and can be optionally sent to a server as JSON objects.
The logger provides a few more [configuration options](#configuring-logger), most notably the inputs to log data from.

This library is a part of the Behametrics project dealing with the research of behavioral biometrics in the context of
user authentication and improving user experience.


## Requirements

* Android SDK version 19 or later
* [AndroidX](https://developer.android.com/jetpack/androidx) library.
  If you use the [Support](https://developer.android.com/topic/libraries/support-library) library in your application,
  you will need to [migrate to AndroidX](https://developer.android.com/jetpack/androidx/migrate) to use this logger.


## Installation

Add the JitPack repository to your root `build.gradle` file:
```groovy
allprojects {
    repositories {
        // Other repositories here...

        maven { url "https://jitpack.io" }
    }
}
```

Add the library dependency to your `app/build.gradle` file:
```groovy
dependencies {
    // Other dependencies here...

    implementation 'com.gitlab.behametrics:logger-android:x.x.x'
}
```

Latest version: [![](https://jitpack.io/v/com.gitlab.behametrics/logger-android.svg)](https://jitpack.io/#com.gitlab.behametrics/logger-android)


## Usage

To start logging data in the entire application, call `Logger.start(this)` at the beginning of the `onCreate()` method
in your main activity.

##### Kotlin
```kotlin
import com.behametrics.logger.Logger

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.start(this)
    }
}
```

##### Java
```java
import com.behametrics.logger.Logger;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.start(this);
    }
}
```

And that's it!
Now the logger collects data from all activities in your application.

Each logged event contains the input type, a timestamp in nanoseconds and a session identifier
(useful for distinguishing e.g. users or application sessions for the same user),
and additional input-specific fields such as touch coordinates and size for the touch screen
or device acceleration along the *x*, *y* and *z* axes for the accelerometer.

To stop logging, simply call `Logger.stop(this)` inside any activity, or do not call `stop` at all if you wish to keep
logging in the entire application.

For some inputs, particularly touch screen and sensors, logging is paused when the application is paused (turning off
the display, switching to another application, etc.) and resumed once the application is resumed.
Logging sensor data in the background can be enabled via the [configuration](#configuring-logger).
Touch data cannot be logged in the background on Android for security reasons.

## Configuring Logger

To customize logging behavior, create a file named `config.properties` in `app/src/main/assets`.
For possible entries and their values, refer to the
[default `config.properties`](https://gitlab.com/behametrics/logger-android/blob/master/logger/src/main/assets/config.properties) file for the logger.

You only need to specify entries that you want changed, other entries will use their default values.
A sound practice is to copy the contents of the entire default config to your `config.properties`
and then adjust values as desired.

## Limitations

See the [wiki section](https://gitlab.com/behametrics/logger-android/wikis/Home#limitations-and-workarounds) on limitations and potential workarounds.

## Additional Information

For more detailed information about usage, see:
* [wiki](https://gitlab.com/behametrics/logger-android/wikis/home),
* [API documentation](https://behametrics.gitlab.io/logger-android/).

For details about the format of input events, consult the
[Behametrics specification](https://gitlab.com/behametrics/specification/blob/master/Specification.md#input-events).

## License

This software is licensed under the [MIT License](LICENSE).
