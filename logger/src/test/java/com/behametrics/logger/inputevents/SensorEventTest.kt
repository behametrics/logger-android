package com.behametrics.logger.inputevents

import com.google.gson.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

// NOTE: Gson assumes floats by default for all numeric types. Therefore, expected integers (such as
// timestamps) are expressed as floats. A custom deserializer for testing purposes is not
// implemented yet due to laziness.

internal class SensorEventTest {

    private lateinit var sensorEvent: SensorEvent

    private val gson = Gson()

    @ParameterizedTest
    @MethodSource("getInputHeaderParams")
    fun getInputHeader(inputName: String, values: FloatArray, expectedHeader: Array<String>) {
        sensorEvent = SensorEvent(inputName, "123-456", 100, values)

        assertEquals(expectedHeader.joinToString(","), sensorEvent.getInputHeader(","))
    }

    @ParameterizedTest
    @MethodSource("toJsonParams")
    fun toJson(
        inputName: String,
        values: FloatArray,
        expectedInputName: String,
        expectedValues: FloatArray,
        expectedHeader: Array<String>
    ) {
        sensorEvent = SensorEvent(inputName, "123-456", 100, values)

        val expectedContent = mutableMapOf<String, Any>()
        val expectedOutput = mutableMapOf(
            "input" to expectedInputName,
            "session_id" to "123-456",
            "timestamp" to 100.0,
            "content" to expectedContent
        )

        for (i in 0 until expectedValues.size) {
            expectedContent[expectedHeader[i]] = expectedValues[i].toDouble()
        }

        val actualOutput = gson.fromJson(sensorEvent.toJson(), Map::class.java)

        assertEquals(expectedOutput.keys, actualOutput.keys)

        for (entry in expectedOutput.entries) {
            if (entry.value is Double && actualOutput[entry.key] is Double) {
                assertEquals(entry.value as Double, actualOutput[entry.key] as Double, 0.01)
            } else {
                assertEquals(entry.value, actualOutput[entry.key])
            }
        }
    }

    companion object {
        @JvmStatic
        fun getInputHeaderParams(): Array<Any?> {
            return arrayOf(
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    arrayOf("input", "session_id", "timestamp", "x", "y", "z")),
                arrayOf<Any?>(
                    "unknown",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    arrayOf("input", "session_id", "timestamp"))
            )
        }

        @JvmStatic
        fun toJsonParams(): Array<Any?> {
            val unknownFieldPrefix = SensorEvent.UNKNOWN_EVENT_FIELD_PREFIX

            return arrayOf(
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    "sensor_accelerometer",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    arrayOf("x", "y", "z")),
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(0.45f, 9.81f),
                    "sensor_accelerometer",
                    floatArrayOf(0.45f, 9.81f),
                    arrayOf("x", "y")),
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(0.45f),
                    "sensor_accelerometer",
                    floatArrayOf(0.45f),
                    arrayOf("x")),
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(),
                    "sensor_accelerometer",
                    floatArrayOf(),
                    arrayOf<String>()),
                arrayOf<Any?>(
                    "accelerometer",
                    floatArrayOf(0.45f, 9.81f, 4.46f, 7.89f),
                    "sensor_accelerometer",
                    floatArrayOf(0.45f, 9.81f, 4.46f, 7.89f),
                    arrayOf("x", "y", "z", "${unknownFieldPrefix}4")),
                arrayOf<Any?>(
                    "unknown",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    "sensor_unknown",
                    floatArrayOf(0.45f, 9.81f, 4.46f),
                    arrayOf("${unknownFieldPrefix}1", "${unknownFieldPrefix}2", "${unknownFieldPrefix}3"))
            )
        }
    }
}
