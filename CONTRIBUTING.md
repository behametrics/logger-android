# Contributing to Behametrics - Android Logger

Thank you for taking your time to contribute to Behametrics - Android Logger.
To help improve the project, we would like you to follow guidelines specified in this document.

## Submitting Changes

1. Make sure your proposed change (new feature, bug fix) is not already implemented (by searching the documentation, closed issues or merge requests).
2. Fork the repository.
3. Create a new, appropriately named branch:

       git checkout -b log-sensor-accuracy master

4. Commit your changes to the new branch.
5. Push your changes to the remote repository:

       git push origin log-sensor-accuracy

6. Create a new merge request on GitLab with `master` as the target branch.

## Coding Conventions

### Kotlin

The `logger` module is written entirely in Kotlin.

If not specified otherwise in this section:
* Use the default lint settings in Android Studio.
* Write code with the [recommended Kotlin conventions](https://kotlinlang.org/docs/reference/coding-conventions.html).

Avoid using the `lateinit` keyword in implementation.
If feasible, make properties and variables initialize at the time of declaration, preferably with a non-null type.

Use the `lateinit` keyword in JUnit tests to postpone property initialization if using a test setup method annotated with `@Before` (JUnit4) or `@BeforeEach` (JUnit5).

Avoid using the not-null assertion operator (`!!`) which makes the code more prone to unexpected `NullPointerException`s.
If feasible, rewrite the code to use a non-null type (preferred) or perform null checks.

### `Logger` Object

Do not use any property or method from the `Logger` object in the implementation.
The exception are the files in the `com.behametrics.logger.utils.logging` package.

Public methods in the `Logger` object must be annotated with `@JvmStatic` to allow easy usage from Java code.

Public methods in the `Logger` object with default parameter values should be annotated with `@JvmOverloads` to allow easy usage from Java code.

### Adding New Input Loggers

When adding a new input logger:
* Take great care to determine whether creating an input event should copy values from members of an object yielded from the input or a reference can be safely kept.
  A prime example is the `android.hardware.SensorEvent.values` array which is reused.
  The array values must therefore be copied explicitly at the time of instantiating `com.behametrics.logger.inputevents.SensorEvent`.
* Determine the value for `loggingContext` - i.e. whether to use the context belonging to a single `Activity` or the entire application.

### Tests

Any new feature or bug fix should be verified by tests, preferably unit tests or Robolectric tests.

### Documenting Code

All public properties and methods must be documented.
Use the [recommended conventions](https://kotlinlang.org/docs/reference/kotlin-doc.html).
You may also want to take a look at the [existing code docs](https://behametrics.gitlab.io/logger-android/) (written in KDoc) for examples.
