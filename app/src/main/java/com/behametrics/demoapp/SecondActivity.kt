package com.behametrics.demoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.behametrics.logger.Logger
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        backButton.setOnClickListener {
            Logger.log("Back")
            startActivity(Intent(this, MainActivity::class.java))
        }

        finishButton.setOnClickListener {
            Logger.log("Finish")
            finish()
        }
    }
}
