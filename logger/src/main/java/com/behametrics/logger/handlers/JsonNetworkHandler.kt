package com.behametrics.logger.handlers

import android.util.Log
import com.behametrics.logger.inputevents.InputEvent
import com.google.gson.JsonElement
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

/**
 * Class responsible for sending logged events as JSON objects to the server specified by the URL
 * having the format `http[s]://serverUrl/endpointRelativeUrl`.
 *
 * HTTP packets being sent contain the specified [loggerHeader] to identify the sent events as
 * coming from this logger implementation.
 *
 * JSON objects are sent in a separate thread in batches of size [inputEventBatchSize]. The batch is
 * sent if full or after a time period of [inputEventMaxHandlingDelayMillis] milliseconds when no
 * new JSON object was added to the batch.
 */
class JsonNetworkHandler @JvmOverloads constructor(
    serverUrl: String,
    private val endpointRelativeUrl: String,
    private val loggerHeader: String,
    inputEventBatchSize: Int = 100,
    inputEventMaxHandlingDelayMillis: Long = 500
) : InputEventHandler {

    private var service: InputEventSendService? = null
    private val headers = mapOf(
        "Content-type" to "application/json",
        "Logger" to loggerHeader
    )

    private val subject = BehaviorSubject.create<InputEvent>()
    private var disposable: Disposable? = null

    init {
        var retrofit: Retrofit? = null

        try {
            retrofit = Retrofit.Builder()
                .baseUrl(serverUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        } catch(e: IllegalArgumentException) {
            Log.e("JsonNetworkHandler", "Could not initialize handler: $e")
        }

        if (retrofit != null) {
            service = retrofit.create(InputEventSendService::class.java)

            disposable = subject
                .subscribeOn(Schedulers.io())
                .map { it.toJson() }
                .buffer(inputEventMaxHandlingDelayMillis, TimeUnit.MILLISECONDS, inputEventBatchSize)
                .subscribeBy(
                    onNext = { sendJsonObjects(it) },
                    onError = { it.printStackTrace() }
                )
        }
    }

    override fun handle(inputEvent: InputEvent) {
        if (service == null) {
            return
        }

        subject.onNext(inputEvent)
    }

    private fun sendJsonObjects(inputEventJsonObjects: List<JsonElement>) {
        if (inputEventJsonObjects.isEmpty()) {
            return
        }

        val call = service?.sendInputEvent(endpointRelativeUrl, headers, inputEventJsonObjects)
        call?.enqueue(EmptyCallback())
    }
}

internal interface InputEventSendService {
    @POST("/{path}")
    fun sendInputEvent(
        @Path("path") endpointRelativeUrl: String,
        @HeaderMap headers: Map<String, String>,
        @Body inputEventJsonObjects: List<@JvmSuppressWildcards JsonElement>
    ): Call<ResponseBody>
}

internal class EmptyCallback : Callback<ResponseBody> {

    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {}

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {}
}
