package com.behametrics.logger.utils.maps.initializers

import com.behametrics.logger.utils.maps.MemberNameValueMap

/** Interface for initializing [MemberNameValueMap] instances of type [T]. */
interface MapInitializer<T> {
    /**
     * Initializes the [map] given the [classWithFields] to retrieve static class member names and
     * values from. The implementation is responsible for selecting member names and values to be
     * put into the [map].
     */
    fun initMap(map: MemberNameValueMap<T>, classWithFields: Class<*>)
}
