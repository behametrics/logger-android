package com.behametrics.logger.callbacks

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.behametrics.logger.config.InputLoggerManager
import com.behametrics.logger.inputloggers.InputLogger

/**
 * Class responsible for starting and stopping input loggers via [inputLoggerManager] when the
 * display goes on and off, respectively.
 */
class ScreenOnOffReceiver(private val inputLoggerManager: InputLoggerManager) : BroadcastReceiver() {

    private val intentFilter = IntentFilter()

    init {
        intentFilter.addAction(Intent.ACTION_SCREEN_ON)
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
    }

    /**
     * Register this receiver for the specified [context].
     */
    fun register(context: Context) {
        context.registerReceiver(this, intentFilter)
    }

    /**
     * Unregister this receiver for the specified [context].
     */
    fun unregister(context: Context) {
        context.unregisterReceiver(this)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent == null || context == null) {
            return
        }

        if (intent.action == Intent.ACTION_SCREEN_ON) {
            inputLoggerManager.startLogging(context) { it.scope == InputLogger.Scopes.DEVICE_DISPLAY_ON }
        } else if (intent.action == Intent.ACTION_SCREEN_OFF) {
            inputLoggerManager.stopLogging(context, { it.scope == InputLogger.Scopes.DEVICE_DISPLAY_ON })
        }
    }
}
