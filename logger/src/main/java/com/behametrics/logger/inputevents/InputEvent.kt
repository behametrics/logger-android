package com.behametrics.logger.inputevents

import com.google.gson.JsonElement
import com.google.gson.JsonObject

/**
 * Abstract class representing input events. Each input event type is associated with an
 * [inputName], [sessionId], and a [timestamp].
 */
abstract class InputEvent(
    val inputName: String,
    protected val sessionId: String,
    protected val timestamp: Long
) {

    /**
     * Returns a header containing field names comprising the input event, using the specified
     * [separator].
     */
    fun getInputHeader(separator: String): String {
        return (COMMON_FIELD_NAMES + getContentFieldNames()).joinToString(separator)
    }

    /**
     * Returns a string representing the input event suitable for a comma-separated values (CSV)
     * format, with each field separated by the [separator].
     */
    fun toCsv(separator: String): String {
        return toString(separator) + "\n"
    }

    /** Returns a JSON object representing the input event, suitable to be sent over the network. */
    fun toJson(): JsonElement {
        val json = getCommonFieldsAsJson()

        val content = JsonObject()
        fillJsonContent(content)
        json.add("content", content)

        return json
    }

    /**
     * Returns a string representing the input event, with each field separated by the [separator].
     */
    abstract fun toString(separator: String): String

    protected abstract fun getContentFieldNames(): Array<String>

    protected abstract fun fillJsonContent(content: JsonObject)

    private fun getCommonFieldsAsJson(): JsonObject {
        val json = JsonObject()

        json.addProperty("input", inputName)
        json.addProperty("session_id", sessionId)
        json.addProperty("timestamp", timestamp)

        return json
    }

    companion object {
        @JvmField
        val COMMON_FIELD_NAMES = arrayOf(
            "input",
            "session_id",
            "timestamp"
        )
    }
}
