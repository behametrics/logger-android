package com.behametrics.logger.utils.maps

import android.hardware.SensorManager
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer

private const val MEMBER_NAME_PREFIX = "SENSOR_DELAY_"

/**
 * A map of sensor delay names to delay types and vice versa according to [SensorManager]. All
 * static members of the [SensorManager] class prefixed with [MEMBER_NAME_PREFIX] are included in
 * the map.
 */
class SensorDelayMap : MemberNameValueMap<Int>(
    PrefixBasedMapInitializer(MEMBER_NAME_PREFIX), SensorManager::class.java)
