package com.behametrics.logger.utils.maps

import android.view.MotionEvent
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer

private const val MEMBER_NAME_PREFIX = "ACTION_"

/**
 * A map of [MotionEvent] action names to action types and vice versa according to [MotionEvent].
 * All static members of the [MotionEvent] class prefixed with [MEMBER_NAME_PREFIX] are included in
 * the map.
 */
class MotionEventActionMap : MemberNameValueMap<Int>(
    PrefixBasedMapInitializer(MEMBER_NAME_PREFIX), MotionEvent::class.java)
