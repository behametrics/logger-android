package com.behametrics.demoapp

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.widget.*
import com.behametrics.demoapp.fragments.ListFragment
import com.behametrics.logger.Logger
import com.behametrics.logger.utils.logging.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.start(this)

        setContentView(R.layout.activity_main)

        buttonSwitchActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }

        buttonStartNewSession.setOnClickListener {
            Logger.startNewSession()
        }

        buttonShowDialogFragment.setOnClickListener {
            showDialogFragment()
        }

        buttonShowDialog.setOnClickListener {
            showEditDialog()
        }

        buttonShowPopupWindow.setOnClickListener {
            showPopupWindow()
        }
    }

    private fun showDialogFragment() {
        val dialogFragmentTag = "dialog"
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        val previousFragment = supportFragmentManager.findFragmentByTag(dialogFragmentTag)
        if (previousFragment != null) {
            fragmentTransaction.remove(previousFragment)
        }

        fragmentTransaction.addToBackStack(null)

        ListFragment().show(fragmentTransaction, dialogFragmentTag)
    }

    private fun showEditDialog() {
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL

        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Test Input")
        alertDialogBuilder.setMessage("Enter arbitrary text: ")
        alertDialogBuilder.setView(input)

        alertDialogBuilder.show().startTouchLogging()
    }

    private fun showPopupWindow() {
        val popupWindow = PopupWindow(this)

        val layout = LinearLayout(this)
        layout.orientation = LinearLayout.VERTICAL

        val textView = TextView(this)
        textView.setText(R.string.example_text)
        textView.width = 500
        textView.height = 500
        layout.addView(textView)

        val buttonDismiss = Button(this)
        buttonDismiss.setText(R.string.dismiss)
        buttonDismiss.setOnClickListener { popupWindow.dismiss() }

        layout.addView(buttonDismiss)

        popupWindow.contentView = layout
        popupWindow.showAtLocation(mainActivityLayout, Gravity.CENTER, 0, 0)

        popupWindow.startTouchLogging()
    }
}
