package com.behametrics.logger.config

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class ModifiableConfigTest {

    private lateinit var config: ModifiableConfig

    @BeforeEach
    fun setUp() {
        config = ModifiableConfig()
    }

    @Test
    fun getStringDefaultEntry() {
        assertFalse(config.getStringEntry("Inputs").isEmpty())
    }

    @ParameterizedTest
    @MethodSource("getStringEntryParams")
    fun getStringEntry(inputValue: String, expectedValue: String) {
        config.setEntry("testEntry", inputValue)
        assertEquals(expectedValue, config.getStringEntry("testEntry"))
    }

    @Test
    fun getStringEntry_NonExistentEntry() {
        assertEquals("", config.getStringEntry("nonExistentEntry"))
    }

    @ParameterizedTest
    @MethodSource("getIntEntryParams")
    fun getIntEntry(inputValue: String, expectedValue: Int) {
        config.setEntry("testEntry", inputValue)
        assertEquals(expectedValue, config.getIntEntry("testEntry"))
    }

    @Test
    fun getIntEntry_NonExistentEntry() {
        assertEquals(0, config.getIntEntry("nonExistentEntry"))
    }

    @ParameterizedTest
    @MethodSource("getBooleanEntryParams")
    fun getBooleanEntry(inputValue: String, expectedValue: Boolean) {
        config.setEntry("testEntry", inputValue)
        assertEquals(expectedValue, config.getBooleanEntry("testEntry"))
    }

    @Test
    fun getBooleanEntry_NonExistentEntry() {
        assertEquals(false, config.getBooleanEntry("nonExistentEntry"))
    }

    @ParameterizedTest
    @MethodSource("getStringArrayEntryParams")
    fun getStringArrayEntry(inputValue: String, separator: String?, expectedValue: Array<String>) {
        config.setEntry("testEntry", inputValue)
        assertArrayEquals(expectedValue, config.getStringArrayEntry("testEntry", separator))
    }

    @ParameterizedTest
    @MethodSource("getStringArrayEntryWithoutWhitespaceTrimmingParams")
    fun getStringArrayEntryWithoutWhitespaceTrimming(inputValue: String, separator: String?, expectedValue: Array<String>) {
        config.setEntry("testEntry", inputValue)
        assertArrayEquals(expectedValue, config.getStringArrayEntry("testEntry", separator, false))
    }

    companion object {
        @JvmStatic
        fun getStringEntryParams() = arrayOf<Any?>(
            arrayOf<Any?>("value", "value"),
            arrayOf<Any?>("", "")
        )

        @JvmStatic
        fun getIntEntryParams() = arrayOf<Any?>(
            arrayOf<Any?>("1", 1),
            arrayOf<Any?>("10", 10),
            arrayOf<Any?>("-1", -1),
            arrayOf<Any?>("", 0),
            arrayOf<Any?>("invalidInteger", 0)
        )

        @JvmStatic
        fun getBooleanEntryParams() = arrayOf<Any?>(
            arrayOf<Any?>("true", true),
            arrayOf<Any?>("True", true),
            arrayOf<Any?>("TRUE", true),
            arrayOf<Any?>("false", false),
            arrayOf<Any?>("False", false),
            arrayOf<Any?>("FALSE", false),
            arrayOf<Any?>("", false),
            arrayOf<Any?>("randomText", false)
        )

        @JvmStatic
        fun getStringArrayEntryParams() = arrayOf<Any?>(
            arrayOf<Any?>("one", ",", arrayOf("one")),
            arrayOf<Any?>("one,two,three", ",", arrayOf("one", "two", "three")),
            arrayOf<Any?>("one.two", ".", arrayOf("one", "two")),
            arrayOf<Any?>("", ",", arrayOf("")),
            arrayOf<Any?>("one,two", "", arrayOf("one,two")),
            arrayOf<Any?>("one,two", null, arrayOf("one,two")),
            arrayOf<Any?>("one,,two", ",", arrayOf("one", "", "two")),
            arrayOf<Any?>("one,two,", ",", arrayOf("one", "two", "")),
            arrayOf<Any?>(",one,two", ",", arrayOf("", "one", "two")),
            arrayOf<Any?>(" one", ",", arrayOf("one")),
            arrayOf<Any?>("one ", ",", arrayOf("one")),
            arrayOf<Any?>("  one ", ",", arrayOf("one")),
            arrayOf<Any?>("one, ,two", ",", arrayOf("one", "", "two")),
            arrayOf<Any?>("one,two, ", ",", arrayOf("one", "two", "")),
            arrayOf<Any?>(" ,one,two", ",", arrayOf("", "one", "two")),
            arrayOf<Any?>("one, two", ",", arrayOf("one", "two")),
            arrayOf<Any?>("one, two", ", ", arrayOf("one", "two")),
            arrayOf<Any?>("one,  two", ", ", arrayOf("one", "two")),
            arrayOf<Any?>(" ", ",", arrayOf(""))
        )

        @JvmStatic
        fun getStringArrayEntryWithoutWhitespaceTrimmingParams() = arrayOf<Any?>(
            arrayOf<Any?>("one", ",", arrayOf("one")),
            arrayOf<Any?>("one,two", ",", arrayOf("one", "two")),
            arrayOf<Any?>("one,two,three", ",", arrayOf("one", "two", "three")),
            arrayOf<Any?>("one,,two", ",", arrayOf("one", "", "two")),
            arrayOf<Any?>("one,two,", ",", arrayOf("one", "two", "")),
            arrayOf<Any?>(",one,two", ",", arrayOf("", "one", "two")),
            arrayOf<Any?>(",one,two", ",", arrayOf("", "one", "two")),
            arrayOf<Any?>("one, two", ",", arrayOf("one", " two")),
            arrayOf<Any?>("one, two", ", ", arrayOf("one", "two"))
        )
    }
}
