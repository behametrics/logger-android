@file:Suppress("DEPRECATION")

package com.behametrics.logger.utils.maps

import android.inputmethodservice.Keyboard
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer

private const val MEMBER_NAME_PREFIX = "KEYCODE_"

/**
 * A map of [Keyboard] keycode names representing special characters (e.g. shift or delete) to
 * keycode numbers and vice versa according to [Keyboard]. All static members of the [Keyboard]
 * class prefixed with [MEMBER_NAME_PREFIX] are included in the map.
 */
class KeyboardKeycodeMap : MemberNameValueMap<Int>(
    PrefixBasedMapInitializer(MEMBER_NAME_PREFIX), Keyboard::class.java)
