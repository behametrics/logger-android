package com.behametrics.logger.callbacks

import android.os.Build
import android.view.*
import android.view.accessibility.AccessibilityEvent
import com.behametrics.logger.inputloggers.TouchLogger

/**
 * Class capturing touch events and dispatching the events to the [touchLogger].
 *
 * This class is meant to be used as a wrapper over the existing [Window.Callback] instance attached
 * to an activity (specified by the [callback] parameter).
 */
class TouchEventTracker(
    private val touchLogger: TouchLogger, val callback: Window.Callback
) : Window.Callback {

    override fun dispatchTouchEvent(motionEvent: MotionEvent?): Boolean {
        touchLogger.processTouchEvent(motionEvent)
        return callback.dispatchTouchEvent(motionEvent)
    }

    override fun dispatchGenericMotionEvent(motionEvent: MotionEvent?): Boolean {
        return callback.dispatchGenericMotionEvent(motionEvent)
    }

    override fun dispatchKeyEvent(keyEvent: KeyEvent?): Boolean {
        return callback.dispatchKeyEvent(keyEvent)
    }

    override fun dispatchKeyShortcutEvent(keyEvent: KeyEvent?): Boolean {
        return callback.dispatchKeyShortcutEvent(keyEvent)
    }

    override fun dispatchPopulateAccessibilityEvent(accessibilityEvent: AccessibilityEvent?): Boolean {
        return callback.dispatchPopulateAccessibilityEvent(accessibilityEvent)
    }

    override fun dispatchTrackballEvent(motionEvent: MotionEvent?): Boolean {
        return callback.dispatchTrackballEvent(motionEvent)
    }

    override fun onActionModeStarted(actionMode: ActionMode?) {
        callback.onActionModeStarted(actionMode)
    }

    override fun onActionModeFinished(actionMode: ActionMode?) {
        callback.onActionModeFinished(actionMode)
    }

    override fun onAttachedToWindow() {
        callback.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        callback.onDetachedFromWindow()
    }

    override fun onContentChanged() {
        callback.onContentChanged()
    }

    override fun onMenuItemSelected(i: Int, menuItem: MenuItem): Boolean {
        return callback.onMenuItemSelected(i, menuItem)
    }

    override fun onMenuOpened(i: Int, menu: Menu): Boolean {
        return callback.onMenuOpened(i, menu)
    }

    override fun onCreatePanelView(i: Int): View? {
        return callback.onCreatePanelView(i)
    }

    override fun onCreatePanelMenu(i: Int, menu: Menu): Boolean {
        return callback.onCreatePanelMenu(i, menu)
    }

    override fun onPreparePanel(i: Int, view: View?, menu: Menu): Boolean {
        return callback.onPreparePanel(i, view, menu)
    }

    override fun onPanelClosed(i: Int, menu: Menu) {
        callback.onPanelClosed(i, menu)
    }

    override fun onSearchRequested(): Boolean {
        return callback.onSearchRequested()
    }

    override fun onSearchRequested(searchEvent: SearchEvent?): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            callback.onSearchRequested(searchEvent)
        } else {
            callback.onSearchRequested()
        }
    }

    override fun onWindowAttributesChanged(layoutParams: WindowManager.LayoutParams?) {
        callback.onWindowAttributesChanged(layoutParams)
    }

    override fun onWindowFocusChanged(b: Boolean) {
        callback.onWindowFocusChanged(b)
    }

    override fun onWindowStartingActionMode(actionModeCallback: ActionMode.Callback?): ActionMode? {
        return callback.onWindowStartingActionMode(actionModeCallback)
    }

    override fun onWindowStartingActionMode(actionModeCallback: ActionMode.Callback?, i: Int): ActionMode? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            callback.onWindowStartingActionMode(actionModeCallback, i)
        } else {
            callback.onWindowStartingActionMode(actionModeCallback)
        }
    }
}
