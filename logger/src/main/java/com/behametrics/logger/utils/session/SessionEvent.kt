package com.behametrics.logger.utils.session

/**
 * Class representing events emitted by [SessionManager] when the session ID has been changed.
 *
 * [currentSessionId] represents the session ID after the change, [previousSessionId] represents the
 * session ID prior to the change.
 *
 * @see SessionManager
 * @see SessionEventListener
 */
class SessionEvent(val currentSessionId: String, val previousSessionId: String)
