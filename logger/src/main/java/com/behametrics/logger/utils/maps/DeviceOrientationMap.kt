package com.behametrics.logger.utils.maps

import android.content.res.Configuration
import com.behametrics.logger.utils.maps.initializers.PrefixBasedMapInitializer

private const val MEMBER_NAME_PREFIX = "ORIENTATION_"

/**
 * A map of names describing device orientation to orientation types and vice versa according to
 * [Configuration]. All static members of the [Configuration] class prefixed with
 * [MEMBER_NAME_PREFIX] are included in the map.
 */
class DeviceOrientationMap : MemberNameValueMap<Int>(
    PrefixBasedMapInitializer(MEMBER_NAME_PREFIX), Configuration::class.java)
