# 0.5.0
* Added support for logging keystroke data and touch data from custom keyboards.
* Provided a custom icon for the demo app.

# 0.4.0

* Orientation events are now logged when the session ID changes.
* Multiple unknown sensors are now properly distinguished (by their numeric ID assigned by a mobile device).
* Added API documentation.
* Updated references to the Behametrics project.

# 0.3.0

* Fixed a fatal bug causing sensor events to have the same values.
* Standardized format of input events for JSON and CSV formats according to the [Behametrics specification](https://gitlab.com/behametrics/specification/blob/release/Specification.md).
* Allowed logging data from multiple activities in the resumed state at once, providing compatibility with Android Q (API 29) and above.
* For Android Q (API 29) and above, allowed logging raw touch X and Y coordinates from touch pointers other than the first pointer.
* Added option to log data while the application is in the foreground or background as long as the device display is turned on.
* Fixed `Logger.start()` with a single parameter not available in Java code as should have been according to the documentation.

# 0.2.0

* Revised starting and stopping logging.
  Removed `Logger.init()` and made `Logger.start()` and `Logger.stop()` work properly across activities.
* Fixed logging of events when transitioning between activities (particularly sensor and lifecycle events).
* Device orientation events are now logged in the background as well.
* Allowed configuring sensor events to be logged in the background.
* Improved performance of writing CSV files and debug outputs by performing work in separate threads.


# 0.1.0

Initial release.
