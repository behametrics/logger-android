@file:Suppress("DEPRECATION")

package com.behametrics.logger.utils.logging

import android.inputmethodservice.InputMethodService
import android.inputmethodservice.KeyboardView

/**
 * Class responsible for logging individual key presses and key releases.
 *
 * This abstract class allows custom keyboards to log keystroke data. Instead of subclassing
 * [InputMethodService] and [KeyboardView.OnKeyboardActionListener], subclass this class instead.
 *
 * Only [onPress] and [onRelease] methods from [KeyboardView.OnKeyboardActionListener] contain
 * actual code, other methods are empty. Therefore, when overriding these methods, invoke
 * `super.onPress(primaryCode)` and `super.onRelease(primaryCode)` at the beginning of these
 * methods.
 */
abstract class KeyboardServiceWithLogging
    : InputMethodService(), KeyboardView.OnKeyboardActionListener {

    override fun onPress(primaryCode: Int) {
        logKeyPress(primaryCode)
    }

    override fun onRelease(primaryCode: Int) {
        logKeyRelease(primaryCode)
    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
    }

    override fun onText(text: CharSequence?) {
    }

    override fun swipeLeft() {
    }

    override fun swipeRight() {
    }

    override fun swipeUp() {
    }

    override fun swipeDown() {
    }
}
