package com.behametrics.logger

import android.app.Activity
import android.content.Context
import com.behametrics.logger.callbacks.ActivityLifecycleTracker
import com.behametrics.logger.callbacks.ScreenOnOffReceiver
import com.behametrics.logger.config.*
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputloggers.InputLogger
import com.behametrics.logger.utils.session.SessionManager
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Object providing high-level API for managing data logging (starting, stopping, modifying) in an
 * Android application.
 */
object Logger {
    private var isLoggerInitialized = false
    private val callbacksRegistered = AtomicBoolean(false)

    private val sessionManager = SessionManager()
    private val inputEventDispatcher = InputEventDispatcher()

    /**
     * Provides access to individual input loggers. Normally, you do not need to start/stop
     * individual loggers and you should use [Logger] to start/stop all input loggers at once.
     *
     * For widgets in which logging cannot be started automatically (such as touch logging in
     * dialogs), use functions from the [com.behametrics.logger.utils.logging] package for
     * convenience.
     */
    @JvmStatic
    val inputLoggerManager = InputLoggerManager(inputEventDispatcher, sessionManager)

    private val activityLifecycleTracker = ActivityLifecycleTracker(inputLoggerManager)
    private val screenOnOffReceiver = ScreenOnOffReceiver(inputLoggerManager)

    /**
     * Provides access to configuration entries loaded from the `assets/config.properties` file and
     * allows their modification.
     */
    @JvmField
    val config = ModifiableConfig()

    /** @see DefaultConfig */
    @JvmField
    val defaultConfig = DefaultConfig

    /**
     * Starts logging data in the application. To stop logging, call [stop]. Consecutive calls to
     * this method without first calling [stop] will have no effect.
     *
     * The [activity] passed is used to initialize tracking changes in the lifecycle of all
     * activities in the application, via [Activity.getApplication] and
     * [Context.getApplicationContext]. Resources from the [activity] itself are used only until the
     * activity enters the [Activity.onPause] stage, thereby avoiding potential memory leaks.
     *
     * By default, the logger starts logging from all inputs immediately. In case you need to
     * postpone logging activity-wide inputs until an activity reaches [Activity.onPause], set
     * [startAll] to false. This is important if you need to call [start] before calling
     * `super.onCreate()` inside [Activity.onCreate], which would otherwise result in duplicate
     * touch logs.
     */
    @JvmStatic
    @JvmOverloads
    fun start(activity: Activity, startAll: Boolean = true) {
        initLogger(activity)

        inputLoggerManager.setInputs(config.getStringArrayEntry("Inputs"))
        inputLoggerManager.sensorLogger.setDelay(config.getStringEntry("SensorLogRate"))
        inputLoggerManager.sensorLogger.setScope(config.getStringEntry("SensorLogScope"))

        val subsetPredicate: SubsetPredicate =
            if (startAll) {
                null
            } else {
                { it.scope == InputLogger.Scopes.DEVICE }
            }

        inputLoggerManager.startLogging(activity, subsetPredicate)

        registerCallbacks(activity)
    }

    /**
     * Stops logging data in the application, using application resources via the specified
     * [activity].
     */
    @JvmStatic
    fun stop(activity: Activity) {
        unregisterCallbacks(activity)

        inputLoggerManager.stopLogging(activity, null, forceStop = true)
    }

    /**
     * Starts a new session. Under the hood, this method generates a new session ID that is attached
     * to logged events.
     *
     * This method is called automatically when calling [start] the first time, thereby initializing
     * the session ID. Subsequent calls to [start] do not refresh the session ID - to do so, you
     * must explicitly call this method.
     *
     * When called explicitly, some inputs may react to changes in session ID, such as by logging
     * the most recent event again (with the new session ID this time).
     */
    @JvmStatic
    fun startNewSession() {
        sessionManager.generateNewSessionId()
    }

    /** Logs a custom event containing the specified fields ([eventData]). */
    @JvmStatic
    fun log(vararg eventData: String) {
        inputLoggerManager.customEventLogger.log(*eventData)
    }

    @JvmStatic
    @Synchronized
    private fun initLogger(activity: Activity) {
        if (!isLoggerInitialized) {
            isLoggerInitialized = true

            config.load(activity.applicationContext.assets)

            startNewSession()

            inputEventDispatcher.handlers.addAll(InputEventHandlerCreator(config).createHandlers(activity.applicationContext))
        }
    }

    @JvmStatic
    private fun registerCallbacks(activity: Activity) {
        if (!callbacksRegistered.getAndSet(true)) {
            activity.application.registerActivityLifecycleCallbacks(activityLifecycleTracker)
            screenOnOffReceiver.register(activity.applicationContext)
        }
    }

    @JvmStatic
    private fun unregisterCallbacks(activity: Activity) {
        if (callbacksRegistered.getAndSet(false)) {
            screenOnOffReceiver.unregister(activity.applicationContext)
            activity.application.unregisterActivityLifecycleCallbacks(activityLifecycleTracker)
        }
    }
}
