package com.behametrics.logger.config

import android.content.Context
import java.io.File

private const val NUMBERED_EXTERNAL_STORAGE_PREFIX = "external_"

/**
 * Class determining the application directory path, depending on the specified storage type
 * (internal or an external storage) from the logger configuration.
 *
 * See the `StorageType` entry in the `assets/config.properties` file for possible values.
 */
class StorageDirPathManager {

    /**
     * Returns the root directory path for the application for data storage, given the supplied
     * [context] and the [storageType].
     */
    fun getStorageDirPath(context: Context, storageType: String): String {
        when {
            storageType == "internal" -> {
                return getInternalStorageDirPath(context)
            }
            storageType == "external" -> {
                val primaryExternalStorageDirPath = getPrimaryExternalStorageDirPath(context)
                if (primaryExternalStorageDirPath != null) {
                    return primaryExternalStorageDirPath
                }
            }
            storageType.startsWith(NUMBERED_EXTERNAL_STORAGE_PREFIX) -> {
                val externalStorageIndexStr = storageType.removePrefix(NUMBERED_EXTERNAL_STORAGE_PREFIX)
                val externalStorageIndex = externalStorageIndexStr.toIntOrNull()

                if (externalStorageIndex != null) {
                    val externalStorageDirPath = getExternalStorageDirPath(context, externalStorageIndex)
                    if (externalStorageDirPath != null) {
                        return externalStorageDirPath
                    }
                }
            }
        }

        return getInternalStorageDirPath(context)
    }

    private fun getInternalStorageDirPath(context: Context): String {
        return context.filesDir.absolutePath
    }

    private fun getPrimaryExternalStorageDirPath(context: Context): String? {
        val externalStorageFile = context.getExternalFilesDir(null)
        return externalStorageFile?.absolutePath
    }

    private fun getExternalStorageDirPath(context: Context, externalStorageIndex: Int): String? {
        val externalStorageFiles = context.getExternalFilesDirs(null)
        if (externalStorageFiles != null) {
            var externalStorageFile: File? = null
            try {
                externalStorageFile = externalStorageFiles[externalStorageIndex]
            } catch (e: ArrayIndexOutOfBoundsException) {
            }

            return if (externalStorageFile != null) {
                externalStorageFile.absolutePath
            } else {
                getPrimaryExternalStorageDirPath(context)
            }
        }

        return null
    }
}
