package com.behametrics.logger.inputevents

import com.behametrics.logger.inputloggers.VariableArgEventLogger
import com.google.gson.Gson
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

// NOTE: Gson assumes floats by default for all numeric types. Therefore, expected integers (such as
// timestamps) are expressed as floats. A custom deserializer for testing purposes is not
// implemented yet due to laziness.

internal class VariableArgEventTest {

    private lateinit var variableArgEvent: VariableArgEvent

    private val gson = Gson()

    @ParameterizedTest
    @MethodSource("toJsonParams")
    fun toJson(eventData: Array<String>, header: Array<String>?, expectedHeader: Array<String>) {
        variableArgEvent = VariableArgEvent(
            "lifecycle", "123-456", 100, eventData, header)

        val expectedContent = mutableMapOf<String, Any>()
        val expectedOutput = mutableMapOf(
            "input" to "lifecycle",
            "session_id" to "123-456",
            "timestamp" to 100.0,
            "content" to expectedContent
        )

        for (pair in expectedHeader.zip(eventData)) {
            expectedContent[pair.first] = pair.second
        }

        assertEquals(expectedOutput, gson.fromJson(variableArgEvent.toJson(), Map::class.java))
    }

    companion object {
        @JvmStatic
        fun toJsonParams(): Array<Any?> {
            val unknownFieldPrefix = VariableArgEventLogger.UNKNOWN_EVENT_FIELD_PREFIX

            return arrayOf(
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity", "started"),
                    arrayOf("object_type", "object_name", "lifecycle_event"),
                    arrayOf("object_type", "object_name", "lifecycle_event")),
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity"),
                    arrayOf("object_type", "object_name", "lifecycle_event"),
                    arrayOf("object_type", "object_name")),
                arrayOf<Any?>(
                    arrayOf("activity"),
                    arrayOf("object_type", "object_name", "lifecycle_event"),
                    arrayOf("object_type")),
                arrayOf<Any?>(
                    arrayOf<String>(),
                    arrayOf("object_type", "object_name", "lifecycle_event"),
                    arrayOf<String>()),
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity", "started"),
                    arrayOf("object_type", "object_name"),
                    arrayOf("object_type", "object_name", "${unknownFieldPrefix}3")),
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity", "started"),
                    arrayOf("object_type"),
                    arrayOf("object_type", "${unknownFieldPrefix}2", "${unknownFieldPrefix}3")),
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity", "started"),
                    arrayOf<String>(),
                    arrayOf("${unknownFieldPrefix}1", "${unknownFieldPrefix}2", "${unknownFieldPrefix}3")),
                arrayOf<Any?>(
                    arrayOf("activity", "MainActivity", "started"),
                    null,
                    arrayOf("${unknownFieldPrefix}1", "${unknownFieldPrefix}2", "${unknownFieldPrefix}3"))
            )
        }
    }
}
