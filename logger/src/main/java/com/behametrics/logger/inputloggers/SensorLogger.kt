package com.behametrics.logger.inputloggers

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.SystemClock
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.SensorEvent
import com.behametrics.logger.utils.session.SessionManager
import com.behametrics.logger.utils.maps.SensorDelayMap
import com.behametrics.logger.utils.maps.SensorTypeMap

/**
 * Class providing logging of events from available sensors.
 *
 * @see SensorEvent
 */
class SensorLogger(
    inputEventDispatcher: InputEventDispatcher,
    sessionManager: SessionManager
) : InputLogger(inputEventDispatcher, sessionManager), SensorEventListener {

    /** Determines the delay (logging frequency) for all sensors. */
    var delay = DEFAULT_SENSOR_DELAY

    private var logAllSensors = false
    private val sensorsToLog = mutableListOf<Int>()

    companion object {
        private val sensorTypeMap = SensorTypeMap()
        private val sensorDelayMap = SensorDelayMap()

        const val SENSOR_NAME_PREFIX = "sensor_"
        const val UNKNOWN_SENSOR_NAME = "unknown"
        const val ALL_SENSORS = "all"

        const val DEFAULT_SENSOR_DELAY = SensorManager.SENSOR_DELAY_GAME

        private const val NANOSECONDS_PER_MILLISECOND: Long = 1000000
        private val startTimeNanos: Long =
            System.currentTimeMillis() * NANOSECONDS_PER_MILLISECOND - SystemClock.elapsedRealtimeNanos()
    }

    override fun startLogging(context: Context) {
        if (logAllSensors) {
            registerAllSensors(context.applicationContext, delay)
        } else {
            registerSensors(context.applicationContext, sensorsToLog, delay)
        }
    }

    override fun stopLogging(context: Context) {
        unregisterAllSensors(context.applicationContext)
    }

    override fun onSensorChanged(sensorEvent: android.hardware.SensorEvent?) {
        if (sensorEvent == null) {
            return
        }

        inputEventDispatcher.dispatch(
            SensorEvent(
                sensorTypeMap.getName(sensorEvent.sensor.type) ?: "${UNKNOWN_SENSOR_NAME}_${sensorEvent.sensor.type}",
                sessionManager.sessionId,
                startTimeNanos + sensorEvent.timestamp,
                // Since `sensorEvent.values` is reused, the array values must be copied into a new
                // array.
                sensorEvent.values.clone()
            )
        )
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    /**
     * Sets the sensors to be logged when next calling [start].
     *
     * See [SensorEvent.SENSOR_HEADERS] for possible sensor names. If [sensorNames] contain an
     * element named [ALL_SENSORS], all sensors will be logged.
     */
    fun setSensors(sensorNames: Iterable<String>) {
        logAllSensors = false
        sensorsToLog.clear()

        for (sensorName in sensorNames) {
            if (sensorName == ALL_SENSORS) {
                logAllSensors = true
                return
            } else {
                if (SensorEvent.RAW_SENSOR_HEADERS.containsKey(sensorName)) {
                    val sensorType = sensorTypeMap.getValue(sensorName)
                    if (sensorType != null) {
                        sensorsToLog.add(sensorType)
                    }
                }
            }
        }
    }

    /**
     * Sets the sensor [delay] (logging frequency) as a string. If the string does not match any
     * valid delay value, use [DEFAULT_SENSOR_DELAY].
     */
    fun setDelay(delay: String) {
        this.delay = sensorDelayMap.getValue(delay) ?: DEFAULT_SENSOR_DELAY
    }

    private fun registerAllSensors(context: Context, delay: Int) {
        val manager = getSensorManager(context)

        for (sensor in manager.getSensorList(Sensor.TYPE_ALL)) {
            manager.registerListener(this, manager.getDefaultSensor(sensor.type), delay)
        }
    }

    private fun registerSensors(context: Context, sensorTypes: Collection<Int>, delay: Int) {
        val manager = getSensorManager(context)
        for (sensorType in sensorTypes) {
            manager.registerListener(this, manager.getDefaultSensor(sensorType), delay)
        }
    }

    private fun unregisterAllSensors(context: Context) {
        val manager = getSensorManager(context)
        val sensors = manager.getSensorList(Sensor.TYPE_ALL)

        for (sensor in sensors) {
            manager.unregisterListener(this, sensor)
        }
    }

    private fun getSensorManager(context: Context): SensorManager {
        return context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }
}
