package com.behametrics.logger.inputloggers

import android.view.MotionEvent
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.utils.session.SessionManager
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach

internal class TouchLoggerTest {

    private lateinit var touchLogger: TouchLogger

    @RelaxedMockK
    private lateinit var mockedInputEventDispatcher: InputEventDispatcher
    @RelaxedMockK
    private lateinit var mockedSessionManager: SessionManager
    @RelaxedMockK
    private lateinit var mockedMotionEvent: MotionEvent

    init {
        MockKAnnotations.init(this)
    }

    @BeforeEach
    fun setUp() {
        touchLogger = TouchLogger(mockedInputEventDispatcher, mockedSessionManager)
    }

    @Test
    fun setScope() {
        touchLogger.scope = InputLogger.Scopes.APPLICATION
        assertEquals(touchLogger.scope, InputLogger.Scopes.APPLICATION)
    }

    @Test
    fun setScope_InvalidValueThrowsException() {
        assertThrows(IllegalArgumentException::class.java) {
            touchLogger.scope = InputLogger.Scopes.DEVICE
        }
    }

    @Test
    fun processTouchEvent_DoesNotDispatchEventForNoLoggedActivities() {
        touchLogger.processTouchEvent(mockedMotionEvent)

        verify(exactly = 0) { mockedInputEventDispatcher.dispatch(any()) }
    }
}
