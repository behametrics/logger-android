package com.behametrics.logger.handlers

import com.behametrics.logger.inputevents.InputEvent

/** Class accepting input events and passing them to available [InputEventHandler] instances. */
class InputEventDispatcher {

    /** List of active handlers attached to the dispatcher. */
    val handlers = mutableListOf<InputEventHandler>()

    /** Passes the [inputEvent] to all available [handlers] in this dispatcher. */
    fun dispatch(inputEvent: InputEvent) {
        for (handler in handlers) {
            handler.handle(inputEvent)
        }
    }
}
