package com.behametrics.logger.inputevents

import com.behametrics.logger.inputloggers.OrientationLogger
import com.google.gson.JsonObject

/**
 * Class for events describing changes in device orientation (portrait, landscape).
 *
 * @param sessionId   session ID
 * @param timestamp   time of measurement in nanoseconds
 * @param orientation the new device orientation
 */
class OrientationEvent(
    sessionId: String,
    timestamp: Long,
    private val orientation: String
) : InputEvent(OrientationLogger.INPUT_NAME, sessionId, timestamp) {

    override fun toString(separator: String): String {
        return arrayOf(inputName, sessionId, timestamp, orientation).joinToString(separator)
    }

    override fun getContentFieldNames(): Array<String> {
        return arrayOf(
            "orientation"
        )
    }

    override fun fillJsonContent(content: JsonObject) {
        content.addProperty("orientation", orientation)
    }
}
