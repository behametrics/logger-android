package com.behametrics.logger.utils.session

/** Interface for listening to changes in session ID provided by [SessionManager]. */
interface SessionEventListener {
    fun onSessionChanged(event: SessionEvent)
}
