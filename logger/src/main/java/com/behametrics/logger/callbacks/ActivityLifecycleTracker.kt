package com.behametrics.logger.callbacks

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.behametrics.logger.config.InputLoggerManager
import com.behametrics.logger.inputloggers.InputLogger

/**
 * Class responsible for logging lifecycle changes in activities and automatically starting or
 * stopping application-wide input loggers via [inputLoggerManager] upon resuming or pausing
 * activities, respectively.
 */
class ActivityLifecycleTracker(private val inputLoggerManager: InputLoggerManager) :
    Application.ActivityLifecycleCallbacks {

    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "created")
    }

    override fun onActivityStarted(activity: Activity) {
        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "started")
    }

    override fun onActivityResumed(activity: Activity) {
        inputLoggerManager.startLogging(activity) { it.scope == InputLogger.Scopes.APPLICATION }

        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "resumed")
    }

    override fun onActivityPaused(activity: Activity) {
        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "paused")

        inputLoggerManager.stopLogging(activity, { it.scope == InputLogger.Scopes.APPLICATION })
    }

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {
        inputLoggerManager.lifecycleLogger.log(
            "activity",
            activity.javaClass.name,
            "save_instance_state"
        )
    }

    override fun onActivityStopped(activity: Activity) {
        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "stopped")
    }

    override fun onActivityDestroyed(activity: Activity) {
        inputLoggerManager.lifecycleLogger.log("activity", activity.javaClass.name, "destroyed")
    }
}
