package com.behametrics.logger.utils.maps

import com.behametrics.logger.utils.maps.initializers.MapInitializer
import java.lang.reflect.Field
import java.util.*

/**
 * Class constructing a map of static member names to their values and vice versa for the specified
 * class ([classWithFields]). All static member values should be of the same type.
 */
open class MemberNameValueMap<T>(
    mapInitializer: MapInitializer<T>?, classWithFields: Class<out Any>
) {

    private var nameValueMap: LinkedHashMap<String, T> = LinkedHashMap()
    private var valueNameMap: LinkedHashMap<T, String> = LinkedHashMap()

    /** Set of names in the map. */
    val names = nameValueMap.keys

    /** Set of values in the map. */
    val values = nameValueMap.values

    init {
        mapInitializer?.initMap(this, classWithFields)
    }

    /**
     * Returns member value for the specified member [name]. If the [name] does not exist, the
     * specified [defaultValue] (or `null`) is returned.
     */
    fun getValue(name: String, defaultValue: T? = null): T? {
        return nameValueMap[getProcessedName(name)] ?: defaultValue
    }

    /** Returns true if the member [name] exists in the map, false otherwise. */
    fun containsName(name: String): Boolean {
        return nameValueMap.containsKey(getProcessedName(name))
    }

    /**
     * Returns member name for the specified member [value]. If the [value] does not exist, the
     * specified [defaultName] (or `null`) is returned.
     */
    fun getName(value: T, defaultName: String? = null): String? {
        return valueNameMap[value] ?: defaultName
    }

    /** Returns true if the [value] exists in the map, false otherwise. */
    fun containsValue(value: T): Boolean {
        return valueNameMap.containsKey(value)
    }

    /**
     * Adds an entry given the member [name] and its [value]. Before adding, the [name] is processed
     * according to the [getProcessedName] method.
     */
    fun put(name: String, value: T) {
        val processedName = getProcessedName(name)

        nameValueMap[processedName] = value
        valueNameMap[value] = processedName
    }

    /**
     * Returns a processed member [name]. This method is invoked before accessing the name in the
     * map.
     *
     * The method in this class in particular returns the [name] in lowercase. Subclasses may
     * override this method to apply additional or different processing to the [name].
     */
    fun getProcessedName(name: String): String {
        return name.toLowerCase(Locale.US)
    }

    /**
     * Returns the value of the specified static [field] (member), or `null` if the [field] is not
     * static or otherwise valid.
     */
    @Suppress("UNCHECKED_CAST")
    fun getStaticMemberValue(field: Field): T? {
        return try {
            field.get(null) as T?
        } catch (e: ClassCastException) {
            null
        } catch (e: IllegalAccessException) {
            null
        } catch (e: IllegalArgumentException) {
            null
        } catch (e: NullPointerException) {
            null
        }
    }
}
