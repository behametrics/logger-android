package com.behametrics.logger.inputloggers

import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.VariableArgEvent
import com.behametrics.logger.utils.session.SessionManager

/**
 * Class providing logging of Android activity or fragment lifecycle events (created, started,
 * paused, stopped, ...).
 *
 * @see VariableArgEvent
 */
class LifecycleLogger(inputEventDispatcher: InputEventDispatcher, sessionManager: SessionManager) :
    VariableArgEventLogger(inputEventDispatcher, sessionManager, INPUT_NAME, CONTENT_FIELD_NAMES) {

    companion object {
        const val INPUT_NAME = "lifecycle"

        @JvmField
        val CONTENT_FIELD_NAMES = arrayOf(
            "object_type",
            "object_name",
            "lifecycle_event"
        )
    }

    /**
     * Logs a lifecycle event if logging is enabled (by calling [start]).
     *
     * @param objectType     type of object having a lifecycle, usually `"activity"` or `"fragment"`
     * @param objectName     name of a specific object instance (activity or fragment)
     * @param lifecycleEvent event describing the change in object lifecycle (e.g. `"created"`,
     *   `"stopped"`)
     */
    fun log(objectType: String, objectName: String, lifecycleEvent: String) {
        super.log(objectType, objectName, lifecycleEvent)
    }
}
