package com.behametrics.logger.inputevents

import com.behametrics.logger.inputloggers.VariableArgEventLogger
import com.google.gson.JsonObject
import kotlin.math.min

/**
 * Class describing an event with a variable number of arguments.
 *
 * @param inputName name of the input
 * @param sessionId session ID
 * @param timestamp time of measurement in nanoseconds
 * @param eventData fields describing the event
 * @param header    optional header providing a name for each field in [eventData]
 */
class VariableArgEvent @JvmOverloads constructor(
    inputName: String,
    sessionId: String,
    timestamp: Long,
    private val eventData: Array<out String>,
    private val header: Array<String>? = null
) : InputEvent(inputName, sessionId, timestamp) {

    override fun toString(separator: String): String {
        return arrayOf(inputName, sessionId, timestamp, *eventData).joinToString(separator)
    }

    override fun getContentFieldNames(): Array<String> {
        return header ?: arrayOf()
    }

    override fun fillJsonContent(content: JsonObject) {
        var currentIndex = 0

        if (header != null) {
            for (i in 0 until min(header.size, eventData.size)) {
                content.addProperty(header[i], eventData[i])
                currentIndex += 1
            }
        }

        for (i in currentIndex until eventData.size) {
            content.addProperty(
                "${VariableArgEventLogger.UNKNOWN_EVENT_FIELD_PREFIX}${i + 1}",
                eventData[i]
            )
        }
    }
}
