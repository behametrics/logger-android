package com.behametrics.logger.handlers

import android.util.Log
import com.behametrics.logger.inputevents.InputEvent
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

private const val LOG_TAG = "Logger"

/**
 * Class printing input events in the console as debug logs. Fields are separated by the specified
 * [fieldSeparator].
 */
class DebugLogOutputHandler @JvmOverloads constructor(
    private val fieldSeparator: String = ","
) : InputEventHandler {

    private val subject = BehaviorSubject.create<InputEvent>()
    private var disposable: Disposable? = null

    init {
        disposable = subject
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onNext = { Log.d(LOG_TAG, it.toString(fieldSeparator)) },
                onError = { it.printStackTrace() }
            )
    }

    override fun handle(inputEvent: InputEvent) {
        subject.onNext(inputEvent)
    }
}
