package com.behametrics.logger.inputloggers

import android.app.Activity
import android.content.Context
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.utils.session.SessionEvent
import com.behametrics.logger.utils.session.SessionManager
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.lang.IllegalArgumentException

internal class InputLoggerTest {

    private lateinit var inputLoggerTestImpl: InputLoggerTestImpl
    private lateinit var sessionManager: SessionManager

    @RelaxedMockK
    private lateinit var mockedInputEventDispatcher: InputEventDispatcher
    @RelaxedMockK
    private lateinit var mockedActivity: Activity
    @RelaxedMockK
    private lateinit var secondMockedActivity: Activity
    @RelaxedMockK
    private lateinit var mockedContext: Context

    init {
        MockKAnnotations.init(this)
    }

    @BeforeEach
    fun setUp() {
        sessionManager = spyk(SessionManager())
        inputLoggerTestImpl = spyk(InputLoggerTestImpl(mockedInputEventDispatcher, sessionManager))
    }

    @ParameterizedTest
    @MethodSource("startAndStopParams")
    fun startAndStop(
        scope: InputLogger.Scopes,
        numStartCalls: Int,
        numStopCalls: Int,
        expectedNumInternalStartCalls: Int,
        expectedNumInternalStopCalls: Int,
        expectedIsRunning: Boolean
    ) {
        inputLoggerTestImpl.scope = scope

        repeat(numStartCalls) { inputLoggerTestImpl.start(mockedActivity) }
        repeat(numStopCalls) { inputLoggerTestImpl.stop(mockedActivity) }

        verify(exactly = expectedNumInternalStartCalls) { inputLoggerTestImpl.testStart() }
        verify(exactly = expectedNumInternalStopCalls) { inputLoggerTestImpl.testStop() }

        assertEquals(expectedIsRunning, inputLoggerTestImpl.isRunning)
    }

    @ParameterizedTest
    @MethodSource("startAndStop_MultipleActivitiesParams")
    fun startAndStop_MultipleActivities(
        scope: InputLogger.Scopes,
        loggingContext: InputLogger.LoggingContext,
        expectedIsRunningAfterFirstActivityStop: Boolean,
        expectedActivitiesUnderLoggingContains: Boolean,
        expectedNumInternalStartCalls: Int,
        expectedNumInternalStopCalls: Int
    ) {
        if (loggingContext == InputLogger.LoggingContext.ACTIVITY) {
            inputLoggerTestImpl = spyk(
                InputLoggerTestImplWithActivityLoggingContext(mockedInputEventDispatcher, sessionManager))
        }

        inputLoggerTestImpl.scope = scope

        inputLoggerTestImpl.start(mockedActivity)

        assertTrue(inputLoggerTestImpl.isRunning)
        assertEquals(
            expectedActivitiesUnderLoggingContains,
            inputLoggerTestImpl.activitiesUnderLogging.contains(mockedActivity))

        inputLoggerTestImpl.start(secondMockedActivity)

        assertTrue(inputLoggerTestImpl.isRunning)
        assertEquals(
            expectedActivitiesUnderLoggingContains,
            inputLoggerTestImpl.activitiesUnderLogging.contains(secondMockedActivity))

        inputLoggerTestImpl.stop(mockedActivity)

        assertEquals(expectedIsRunningAfterFirstActivityStop, inputLoggerTestImpl.isRunning)
        assertFalse(inputLoggerTestImpl.activitiesUnderLogging.contains(mockedActivity))

        inputLoggerTestImpl.stop(secondMockedActivity)

        assertFalse(inputLoggerTestImpl.isRunning)
        assertTrue(inputLoggerTestImpl.activitiesUnderLogging.isEmpty())

        verify(exactly = expectedNumInternalStartCalls) { inputLoggerTestImpl.testStart() }
        verify(exactly = expectedNumInternalStopCalls) { inputLoggerTestImpl.testStop() }
    }

    @ParameterizedTest
    @MethodSource("stopWithForceParams")
    fun stopWithForce(
        scope: InputLogger.Scopes,
        loggingContext: InputLogger.LoggingContext,
        numForceStopCalls: Int,
        expectedIsRunning: Boolean,
        expectedIsEmpty: Boolean,
        expectedNumInternalStartCalls: Int,
        expectedNumInternalStopCalls: Int
    ) {
        if (loggingContext == InputLogger.LoggingContext.ACTIVITY) {
            inputLoggerTestImpl = spyk(
                InputLoggerTestImplWithActivityLoggingContext(mockedInputEventDispatcher, sessionManager))
        }

        inputLoggerTestImpl.scope = scope

        inputLoggerTestImpl.start(mockedActivity)
        inputLoggerTestImpl.start(secondMockedActivity)
        repeat(numForceStopCalls) { inputLoggerTestImpl.stop(mockedActivity, true) }

        assertEquals(expectedIsRunning, inputLoggerTestImpl.isRunning)
        assertEquals(expectedIsEmpty, inputLoggerTestImpl.activitiesUnderLogging.isEmpty())

        verify(exactly = expectedNumInternalStartCalls) { inputLoggerTestImpl.testStart() }
        verify(exactly = expectedNumInternalStopCalls) { inputLoggerTestImpl.testStop() }
    }

    @Test
    fun start_ApplicationScope_PassingNonActivityContextThrowsException() {
        inputLoggerTestImpl.scope = InputLogger.Scopes.APPLICATION

        assertThrows(IllegalArgumentException::class.java) { inputLoggerTestImpl.start(mockedContext) }
    }

    @Test
    fun stop_ApplicationScope_PassingNonActivityContextThrowsException() {
        inputLoggerTestImpl.scope = InputLogger.Scopes.APPLICATION
        inputLoggerTestImpl.start(mockedActivity)

        assertThrows(IllegalArgumentException::class.java) { inputLoggerTestImpl.stop(mockedContext) }
    }

    @ParameterizedTest
    @MethodSource("setScopeByNameParams")
    fun setScopeByName(scopeName: String, expectedScope: InputLogger.Scopes) {
        inputLoggerTestImpl.setScope(scopeName)

        assertEquals(expectedScope, inputLoggerTestImpl.scope)
    }

    @ParameterizedTest
    @MethodSource("setScope_TakesEffectOnNextStartParams")
    fun setScope_TakesEffectOnNextStart(
        initialScope: InputLogger.Scopes,
        newScope: InputLogger.Scopes
    ) {
        inputLoggerTestImpl.scope = initialScope

        inputLoggerTestImpl.start(mockedActivity)
        inputLoggerTestImpl.start(secondMockedActivity)

        inputLoggerTestImpl.scope = newScope

        assertEquals(initialScope, inputLoggerTestImpl.currentScope)

        inputLoggerTestImpl.stop(mockedActivity, true)
        inputLoggerTestImpl.start(mockedActivity)

        assertEquals(newScope, inputLoggerTestImpl.currentScope)
    }

    @ParameterizedTest
    @MethodSource("startAndStop_OnSessionChangedIsEmittedByChangingSessionIdParams")
    fun startAndStop_OnSessionChangedIsEmittedByChangingSessionId(
        numStartCalls: Int,
        numStopCalls: Int,
        expectedNumCallsToSessionChange: Int
    ) {
        repeat(numStartCalls) { inputLoggerTestImpl.start(mockedActivity) }
        repeat(numStopCalls) { inputLoggerTestImpl.stop(mockedActivity) }

        sessionManager.generateNewSessionId()

        verify (exactly = expectedNumCallsToSessionChange) {
            inputLoggerTestImpl.testOnSessionChanged()
        }
    }

    companion object {
        @JvmStatic
        fun startAndStopParams() = arrayOf<Any?>(
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 1, 0, 1, 0, true),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 2, 0, 1, 0, true),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 1, 1, 1, 1, false),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 0, 1, 0, 0, false),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 0, 2, 0, 0, false),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, 1, 2, 1, 1, false),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 1, 0, 1, 0, true),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 2, 0, 1, 0, true),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 1, 1, 1, 1, false),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 0, 1, 0, 0, false),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 0, 2, 0, 0, false),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, 1, 2, 1, 1, false)
        )

        @JvmStatic
        fun startAndStop_MultipleActivitiesParams() = arrayOf<Any?>(
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, InputLogger.LoggingContext.APPLICATION, false, false, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, InputLogger.LoggingContext.ACTIVITY, false, false, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.APPLICATION, true, true, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.ACTIVITY, true, true, 2, 2)
        )

        @JvmStatic
        fun stopWithForceParams() = arrayOf<Any?>(
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, InputLogger.LoggingContext.APPLICATION, 1, false, true, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, InputLogger.LoggingContext.ACTIVITY, 1, false, true, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.APPLICATION, 1, false, true, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.ACTIVITY, 1, false, true, 2, 2),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.APPLICATION, 0, true, false, 1, 0),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.ACTIVITY, 0, true, false, 2, 0),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.APPLICATION, 2, false, true, 1, 1),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.LoggingContext.ACTIVITY, 2, false, true, 2, 2)
        )

        @JvmStatic
        fun setScopeByNameParams() = arrayOf<Any?>(
            arrayOf<Any?>("APPLICATION", InputLogger.Scopes.APPLICATION),
            arrayOf<Any?>("application", InputLogger.Scopes.APPLICATION),
            arrayOf<Any?>("device", InputLogger.Scopes.DEVICE),
            arrayOf<Any?>("invalid_value", InputLogger.Scopes.APPLICATION)
        )

        @JvmStatic
        fun setScope_TakesEffectOnNextStartParams() = arrayOf<Any?>(
            arrayOf<Any?>(InputLogger.Scopes.DEVICE, InputLogger.Scopes.APPLICATION),
            arrayOf<Any?>(InputLogger.Scopes.APPLICATION, InputLogger.Scopes.DEVICE)
        )

        @JvmStatic
        fun startAndStop_OnSessionChangedIsEmittedByChangingSessionIdParams() = arrayOf<Any?>(
            arrayOf<Any?>(1, 1, 0),
            arrayOf<Any?>(1, 0, 1),
            arrayOf<Any?>(2, 0, 1),
            arrayOf<Any?>(0, 1, 0),
            arrayOf<Any?>(0, 2, 0),
            arrayOf<Any?>(2, 2, 0),
            arrayOf<Any?>(0, 0, 0)
        )
    }

    internal open class InputLoggerTestImpl(
        inputEventDispatcher: InputEventDispatcher,
        sessionManager: SessionManager
    ) : InputLogger(inputEventDispatcher, sessionManager) {

        override fun startLogging(context: Context) {
            testStart()
        }

        override fun stopLogging(context: Context) {
            testStop()
        }

        override fun onSessionChanged(event: SessionEvent) {
            testOnSessionChanged()
        }

        internal fun testStart() {
        }

        internal fun testStop() {
        }

        internal fun testOnSessionChanged() {
        }
    }

    internal class InputLoggerTestImplWithActivityLoggingContext(
        inputEventDispatcher: InputEventDispatcher,
        sessionManager: SessionManager
    ) : InputLoggerTestImpl(inputEventDispatcher, sessionManager) {

        override val loggingContext = LoggingContext.ACTIVITY
    }
}
