package com.behametrics.logger.config

import android.content.res.AssetManager
import java.io.IOException
import java.util.Properties

private val DEFAULT_CONFIG = mapOf(
    "Inputs" to "touch,orientation,custom,sensor_accelerometer,sensor_gyroscope",
    "SensorLogRate" to "game",
    "SensorLogScope" to "application",
    "Outputs" to "local",
    "StorageType" to "internal",
    "LogsDirPath" to "logs",
    "MinFreeSpaceLimitMB" to "300",
    "ServerUrl" to "",
    "EndpointUrl" to "data",
    "LoggerHeader" to "behametrics/android"
)

/**
 * Class providing access to configuration entries for the library. Upon instantiation, the object
 * is filled with default configuration values. This class also provides convenience methods to
 * parse entries to several primitive types or string arrays.
 *
 * This class internally uses [Properties] for managing configuration entries.
 */
open class Config {

    protected val properties: Properties = Properties()

    init {
        properties.putAll(DEFAULT_CONFIG)
    }

    /**
     * Returns a string entry for the given [key]. Returns the [defaultValue] if the [key] does not
     * exist.
     */
    fun getStringEntry(key: String, defaultValue: String = ""): String {
        return properties.getProperty(key) ?: defaultValue
    }

    /**
     * Returns an integer entry for the given [key]. Returns the [defaultValue] if the [key] does
     * not exist or the entry cannot be converted to an integer.
     */
    fun getIntEntry(key: String, defaultValue: Int = 0): Int {
        return properties.getProperty(key)?.toIntOrNull() ?: defaultValue
    }

    /**
     * Returns a boolean entry for the given [key]. Returns the [defaultValue] if the [key] does not
     * exist. `"true"` (case-insensitive) is converted to true, any other value is converted to
     * false.
     */
    fun getBooleanEntry(key: String, defaultValue: Boolean = false): Boolean {
        return properties.getProperty(key)?.toBoolean() ?: defaultValue
    }

    /**
     * Returns an entry as an array of strings extracted from the [key] containing the specified
     * [separator]. Returns the [defaultValue] if the [key] does not exist.
     *
     * If the [separator] is empty or `null`, the entire entry is returned as a single-element
     * array.
     *
     * If [trimWhitespace] is true, whitespace characters from the beginning and end of each element
     * are removed.
     */
    fun getStringArrayEntry(
        key: String,
        separator: String? = ",",
        trimWhitespace: Boolean = true,
        defaultValue: Array<String> = arrayOf()
    ): Array<String> {
        val entry = properties.getProperty(key) ?: return defaultValue

        if (separator == null || separator.isEmpty()) {
            return arrayOf(entry)
        }

        val elements = entry.split(separator)

        return if (trimWhitespace) {
            elements.map { it.trim() }.toTypedArray()
        } else {
            elements.toTypedArray()
        }
    }
}

/**
 * [Config] class with the following additional features:
 * * loading configuration entries from a file,
 * * modifying configuration entries.
 */
class ModifiableConfig : Config() {

    companion object {
        /** Filename of the default configuration file under the `assets` project folder. */
        const val DEFAULT_CONFIG_FILENAME = "config.properties"
    }

    /**
     * Loads configuration entries from a [fileName] stored in the `assets` folder via the
     * [assetManager]. Any existing entry will be updated with the value specified in the file.
     *
     * The method returns true if the loading proceeded successfully, false otherwise.
     */
    fun load(assetManager: AssetManager, fileName: String = DEFAULT_CONFIG_FILENAME): Boolean {
        try {
            properties.load(assetManager.open(fileName))
        } catch (e: IOException) {
            return false
        }

        return true
    }

    /**
     * Creates a new entry given its [name] and [value], or updates an existing entry matching the
     * specified [name] with the new [value].
     */
    fun setEntry(name: String, value: String) {
        properties.setProperty(name, value)
    }
}

/**
 * Object containing configuration with factory default values. The entries are guaranteed to be
 * unmodifiable.
 */
object DefaultConfig: Config()
