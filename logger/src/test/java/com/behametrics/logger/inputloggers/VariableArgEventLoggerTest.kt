package com.behametrics.logger.inputloggers

import android.app.Activity
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.InputEvent
import com.behametrics.logger.inputevents.VariableArgEvent
import com.behametrics.logger.utils.session.SessionManager
import com.google.gson.Gson
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class VariableArgEventLoggerTest {

    private lateinit var variableArgEventLogger: VariableArgEventLogger

    @RelaxedMockK
    private lateinit var mockedActivity: Activity
    @RelaxedMockK
    private lateinit var mockedInputEventDispatcher: InputEventDispatcher
    @RelaxedMockK
    private lateinit var mockedSessionManager: SessionManager

    @Nested
    inner class WithoutHeaders {
        @BeforeEach
        fun setUp() {
            variableArgEventLogger = VariableArgEventLogger(
                mockedInputEventDispatcher, mockedSessionManager, "custom")
        }

        @Test
        fun logAfterStart() {
            variableArgEventLogger.start(mockedActivity)
            variableArgEventLogger.log("start_of_activity")

            verify(exactly = 1) {
                mockedInputEventDispatcher.dispatch(any())
            }
        }

        @Test
        fun logNoEventWithoutStart() {
            variableArgEventLogger.log("start_of_activity")

            verify(exactly = 0) {
                mockedInputEventDispatcher.dispatch(any())
            }
        }

        @Test
        fun logNoEventAfterStartAndStop() {
            variableArgEventLogger.start(mockedActivity)
            variableArgEventLogger.stop(mockedActivity)
            variableArgEventLogger.log("start_of_activity")

            verify(exactly = 0) {
                mockedInputEventDispatcher.dispatch(any())
            }
        }
    }

    @Nested
    inner class WithHeaders {
        private val gson = Gson()

        private val header = arrayOf("field1", "field2")

        @BeforeEach
        fun setUp() {
            variableArgEventLogger = VariableArgEventLogger(
                mockedInputEventDispatcher, mockedSessionManager, "custom", header)
            variableArgEventLogger.start(mockedActivity)
        }

        @Test
        fun log() {
            val slot = slot<VariableArgEvent>()

            variableArgEventLogger.log("activity", "started")

            verify(exactly = 1) {
                mockedInputEventDispatcher.dispatch(capture(slot))
            }

            assertEquals((InputEvent.COMMON_FIELD_NAMES + header).joinToString(","), slot.captured.getInputHeader(","))

            assertEquals(
                mapOf(
                    "field1" to "activity",
                    "field2" to "started"
                ),
                gson.fromJson(slot.captured.toJson().asJsonObject.get("content"), Map::class.java)
            )
        }
    }
}
