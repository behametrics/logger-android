package com.behametrics.logger.config

import android.app.Activity
import android.content.Context
import com.behametrics.logger.callbacks.FragmentLifecycleCallbackManager
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputloggers.*
import com.behametrics.logger.utils.session.SessionManager
import java.util.concurrent.atomic.AtomicBoolean

private const val CUSTOM_EVENT_INPUT_NAME = "custom"
private const val ALL_INPUTS = "all"

/**
 * Type indicating predicate functions to filter input loggers used for
 * [InputLoggerManager.startLogging].
 */
typealias SubsetPredicate = ((InputLogger) -> Boolean)?

/**
 * Class responsible for starting and stopping input loggers and setting the inputs to be logged.
 */
class InputLoggerManager(
    inputEventDispatcher: InputEventDispatcher, sessionManager: SessionManager
) {

    val touchLogger: TouchLogger = TouchLogger(inputEventDispatcher, sessionManager)
    val sensorLogger: SensorLogger = SensorLogger(inputEventDispatcher, sessionManager)
    val orientationLogger: OrientationLogger = OrientationLogger(
        inputEventDispatcher, sessionManager)
    val customEventLogger: VariableArgEventLogger = VariableArgEventLogger(
        inputEventDispatcher, sessionManager, CUSTOM_EVENT_INPUT_NAME)
    val lifecycleLogger: LifecycleLogger = LifecycleLogger(inputEventDispatcher, sessionManager)
    val keyboardLogger: KeyboardLogger = KeyboardLogger(
        inputEventDispatcher, sessionManager)

    private val inputNameMap = mapOf(
        TouchLogger.INPUT_NAME to touchLogger,
        SensorLogger.SENSOR_NAME_PREFIX to sensorLogger,
        OrientationLogger.INPUT_NAME to orientationLogger,
        CUSTOM_EVENT_INPUT_NAME to customEventLogger,
        LifecycleLogger.INPUT_NAME to lifecycleLogger,
        KeyboardLogger.INPUT_NAME to keyboardLogger
    )

    private val isRunning = AtomicBoolean(false)

    private val fragmentLifecycleCallbackManager = FragmentLifecycleCallbackManager(lifecycleLogger)

    private val runningInputLoggers = mutableListOf<InputLogger>()
    private val inputLoggersToStartNext = mutableListOf<InputLogger>()

    /**
     * Starts logging using the specified [context] from inputs specified in the [setInputs] method.
     *
     * You may control which subsets of loggers to start via the [subsetPredicate] function.
     */
    @JvmOverloads
    fun startLogging(context: Context, subsetPredicate: SubsetPredicate = null) {
        if (!isRunning.getAndSet(true)) {
            runningInputLoggers.clear()
            runningInputLoggers.addAll(inputLoggersToStartNext)
        }

        if (context is Activity) {
            fragmentLifecycleCallbackManager.registerCallbacks(context)
        }

        getInputLoggerSubset(runningInputLoggers, subsetPredicate).forEach { it.start(context) }
    }

    /**
     * Stops logging using the specified [context] for the inputs specified in the [setInputs]
     * method.
     *
     * You may control which subsets of loggers to start via the [subsetPredicate] function.
     *
     * If [forceStop] is true, stop logging unconditionally regardless of the value of
     * [InputLogger.scope] for each input.
     */
    @JvmOverloads
    fun stopLogging(
        context: Context, subsetPredicate: SubsetPredicate = null, forceStop: Boolean = false)
    {
        getInputLoggerSubset(runningInputLoggers, subsetPredicate).forEach {
            it.stop(context, forceStop)
        }

        if (context is Activity) {
            fragmentLifecycleCallbackManager.unregisterCallbacks(context)
        }

        isRunning.set(false)
    }

    /**
     * Sets the specified [inputs] to be logged the next time [startLogging] is called.
     *
     * If [inputs] contain an element named [ALL_INPUTS], all input loggers (including all sensors)
     * are set. Note that logging data from all inputs may result in a significant performance drop
     * in your application.
     *
     * See the `Input` entry in the `assets/config.properties` file for possible values.
     */
    fun setInputs(inputs: Array<String>) {
        inputLoggersToStartNext.clear()
        inputLoggersToStartNext.addAll(getInputLoggers(inputs))

        sensorLogger.setSensors(
            inputs.filter { it.startsWith(SensorLogger.SENSOR_NAME_PREFIX) }
                .map { it.removePrefix(SensorLogger.SENSOR_NAME_PREFIX) })
    }

    internal fun getInputLoggers(inputs: Array<String>): Collection<InputLogger> {
        val inputLoggers = mutableListOf<InputLogger>()
        var sensorLoggerAdded = false

        for (input in inputs) {
            when {
                input.startsWith(SensorLogger.SENSOR_NAME_PREFIX) -> {
                    if (!sensorLoggerAdded) {
                        sensorLoggerAdded = true
                        inputLoggers.add(sensorLogger)
                    }
                }
                input == ALL_INPUTS -> {
                    return inputNameMap.values
                }
                else -> {
                    val inputLogger = inputNameMap[input]
                    if (inputLogger != null && inputLogger !in inputLoggers) {
                        inputLoggers.add(inputLogger)
                    }
                }
            }
        }

        return inputLoggers
    }

    internal fun getInputLoggerSubset(
        inputLoggers: Collection<InputLogger>, subsetPredicate: SubsetPredicate
    ): Collection<InputLogger> {
        return if (subsetPredicate == null) {
            inputLoggers
        } else {
            inputLoggers.filter(subsetPredicate)
        }
    }
}
