# Module logger

Documentation for the Android logger library

# Package com.behametrics.logger

Provides high-level API to control logging in Android apps.

# Package com.behametrics.logger.callbacks

Classes capturing certain application events via callbacks.

# Package com.behametrics.logger.config

Manages logger configuration and provides helper classes to process configuration entries.

# Package com.behametrics.logger.handlers

Classes responsible for formatting and writing/sending input events.

# Package com.behametrics.logger.inputevents

Classes representing input events.

# Package com.behametrics.logger.inputloggers

Classes capturing events from inputs and dispatching the events to handlers.

# Package com.behametrics.logger.session

Classes responsible for managing sessions and providing the means to handle changes in sessions.

# Package com.behametrics.logger.utils

Assorted collection of classes and extension functions supporting the library.

# Package com.behametrics.logger.utils.logging

Helper functions to simplify explicit starting and stopping logging in special cases (such as touch logging in dialogs).

# Package com.behametrics.logger.utils.maps

Classes mapping integer values to variable names (used for some fields in input events).

# Package com.behametrics.logger.utils.maps.initializers

Classes responsible for pre-filling maps in `com.behametrics.logger.utils.maps`.
