package com.behametrics.logger.inputevents

import com.behametrics.logger.inputloggers.SensorLogger
import com.google.gson.JsonObject
import kotlin.math.min

/**
 * Class describing a sensor event.
 *
 * @param sensorName name of the sensor providing the measurement (e.g. `"accelerometer"`,
 *   `"gyroscope"`)
 * @param sessionId  session ID
 * @param timestamp  time of measurement in nanoseconds
 * @param values     array of measured sensor values
 */
class SensorEvent(
    private val sensorName: String,
    sessionId: String,
    timestamp: Long,
    private val values: FloatArray
) : InputEvent(SensorLogger.SENSOR_NAME_PREFIX + sensorName, sessionId, timestamp) {

    override fun toString(separator: String): String {
        return arrayOf(inputName, sessionId, timestamp, values.joinToString(separator)).joinToString(separator)
    }

    override fun getContentFieldNames(): Array<String> {
        return SENSOR_HEADERS[inputName] ?: arrayOf()
    }

    override fun fillJsonContent(content: JsonObject) {
        var currentIndex = 0
        val header = RAW_SENSOR_HEADERS[sensorName]

        if (header != null) {
            for (i in 0 until min(values.size, header.size)) {
                content.addProperty(header[i], values[i])
                currentIndex += 1
            }
        }

        for (i in currentIndex until values.size) {
            content.addProperty("$UNKNOWN_EVENT_FIELD_PREFIX${i+1}", values[i])
        }
    }

    companion object {
        const val UNKNOWN_EVENT_FIELD_PREFIX = "value"

        /** Map of sensor names to arrays of header fields for that sensor. */
        @JvmField
        val RAW_SENSOR_HEADERS = mapOf(
            "accelerometer"
                    to arrayOf("x", "y", "z"),
            "magnetic_field"
                    to arrayOf("x", "y", "z"),
            "gyroscope"
                    to arrayOf("x", "y", "z"),
            "light"
                    to arrayOf("light_level_lux"),
            "pressure"
                    to arrayOf("pressure_hpa"),
            "proximity"
                    to arrayOf("distance_cm"),
            "gravity"
                    to arrayOf("x", "y", "z"),
            "linear_acceleration"
                    to arrayOf("x", "y", "z"),
            "rotation_vector"
                    to arrayOf("x", "y", "z", "magnitude", "accuracy"),
            "orientation"
                    to arrayOf("azimuth", "pitch", "roll"),
            "relative_humidity"
                    to arrayOf("relative_humidity_percent"),
            "ambient_temperature"
                    to arrayOf("degrees_celsius"),
            "magnetic_field_uncalibrated"
                    to arrayOf("x", "y", "z", "iron_bias_x", "iron_bias_y", "iron_bias_z"),
            "game_rotation_vector"
                    to arrayOf("x", "y", "z", "magnitude"),
            "gyroscope_uncalibrated"
                    to arrayOf("x", "y", "z", "drift_x", "drift_y", "drift_z"),
            "pose_6dof"
                    to arrayOf(
                "rotation_x",
                "rotation_y",
                "rotation_z",
                "magnitude",
                "translation_x",
                "translation_y",
                "translation_z",
                "delta_rotation_x",
                "delta_rotation_y",
                "delta_rotation_z",
                "delta_magnitude",
                "delta_translation_x",
                "delta_translation_y",
                "delta_translation_z",
                "sequence_number"
            ),
            "stationary_detect"
                    to arrayOf("is_stationary"),
            "motion_detect"
                    to arrayOf("is_in_motion"),
            "heart_beat"
                    to arrayOf("confidence"),
            "low_latency_off_body_detect"
                    to arrayOf("off_body_state"),
            "accelerometer_uncalibrated"
                    to arrayOf("x", "y", "z", "bias_x", "bias_y", "bias_z")
        )

        /**
         * Map of sensor names with a common prefix to arrays of header fields for that sensor
         * containing additional fields common for all sensors (such as timestamp).
         *
         * This map is suitable to be used for logging data as the prefix avoids ambiguities in
         * identifying inputs and the common fields contain essential information for each sensor
         * event.
         */
        @JvmField
        val SENSOR_HEADERS = getSensorHeaders()

        private fun getSensorHeaders(): Map<String, Array<String>> {
            return RAW_SENSOR_HEADERS.entries.associate {
                getSensorName(it.key) to it.value
            }
        }

        private fun getSensorName(sensorName: String): String {
            return SensorLogger.SENSOR_NAME_PREFIX + sensorName
        }
    }
}
