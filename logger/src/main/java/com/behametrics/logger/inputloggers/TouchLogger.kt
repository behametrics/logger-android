package com.behametrics.logger.inputloggers

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Build
import androidx.fragment.app.DialogFragment
import android.view.MotionEvent
import android.view.Window
import android.widget.PopupWindow
import com.behametrics.logger.callbacks.TouchEventTracker
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.TouchEvent
import com.behametrics.logger.utils.session.SessionManager
import com.behametrics.logger.utils.getNonSensorEventTimestampNanos
import com.behametrics.logger.utils.maps.MotionEventActionMap

/**
 * Class providing logging of touch screen events.
 *
 * This logger only supports logging while the application is active, i.e. the [scope] must have the
 * value [InputLogger.Scopes.APPLICATION]. Any other value results in [IllegalArgumentException]
 * being thrown.
 *
 * When calling [start] or [stop] using a [Context] parameter, only [Activity] instances are allowed
 * to be passed. Otherwise, [IllegalArgumentException] is thrown.
 *
 * @see TouchEvent
 */
class TouchLogger(
    inputEventDispatcher: InputEventDispatcher,
    sessionManager: SessionManager
) : InputLogger(inputEventDispatcher, sessionManager) {

    override val loggingContext = LoggingContext.ACTIVITY

    companion object {
        private val motionEventActionMap = MotionEventActionMap()

        const val INPUT_NAME = "touch"

        private const val TOUCH_DOWN = "down"
        private const val TOUCH_UP = "up"
        private const val TOUCH_MOVE = "move"
        private const val TOUCH_CANCEL = "cancel"
        private const val TOUCH_OTHER = "other"
    }

    override var scope: Scopes = super.scope
        set(value) = if (value == Scopes.APPLICATION) {
            field = value
        } else {
            throw IllegalArgumentException("Touch logger does not support logging outside the application")
        }

    override fun startLogging(context: Context) {
        if (context !is Activity) {
            throw IllegalArgumentException("context must be an Activity instance")
        }

        val activityWindow = context.window ?: return
        start(activityWindow)
    }

    /**
     * Starts logging touch events in the specified [dialog]. Consecutive calls to this method
     * without first calling [stop] with the same dialog will have no effect.
     */
    fun start(dialog: Dialog) {
        val dialogWindow = dialog.window ?: return
        start(dialogWindow)
    }

    /**
     * Starts logging touch events in the specified [dialogFragment]. Consecutive calls to this
     * method without first calling [stop] with the same dialog fragment will have no effect.
     */
    fun start(dialogFragment: DialogFragment) {
        val dialog = dialogFragment.dialog ?: return
        start(dialog)
    }

    /**
     * Starts logging touch events in the specified [dialogFragment]. Consecutive calls to this
     * method without first calling [stop] with the same dialog fragment will have no effect.
     *
     * @param dialogFragment dialog fragment to log touch events in
     */
    @Suppress("DEPRECATION")
    fun start(dialogFragment: android.app.DialogFragment) {
        start(dialogFragment.dialog)
    }

    /**
     * Starts logging touch events in the specified [popupWindow]. Consecutive calls to this method
     * without first calling [stop] with the same popup window will have no effect.
     *
     * Note: If the [popupWindow] already has an [android.view.View.OnTouchListener] instance
     * connected, it will be overridden. To prevent this behavior, manually invoke
     * [processTouchEvent] inside your listener.
     */
    fun start(popupWindow: PopupWindow) {
        popupWindow.setTouchInterceptor { _, event ->
            processTouchEvent(event)
            false
        }
    }

    private fun start(window: Window) {
        if (window.callback !is TouchEventTracker) {
            window.callback = TouchEventTracker(this, window.callback)
        }
    }

    override fun stopLogging(context: Context) {
        if (context !is Activity) {
            throw IllegalArgumentException("context must be an Activity instance")
        }

        val activityWindow = context.window ?: return
        stop(activityWindow)
    }

    /**
     * Stops logging touch events in the specified [dialog]. Calling this method before first
     * calling [start] with the same dialog will have no effect.
     */
    fun stop(dialog: Dialog) {
        val dialogWindow = dialog.window ?: return
        stop(dialogWindow)
    }

    /**
     * Stops logging touch events in the specified [dialogFragment]. Calling this method before
     * first calling [start] with the same dialog fragment will have no effect.
     */
    fun stop(dialogFragment: DialogFragment) {
        val dialog = dialogFragment.dialog ?: return
        stop(dialog)
    }

    /**
     * Stops logging touch events in the specified [dialogFragment]. Calling this method before
     * first calling [start] with the same dialog fragment will have no effect.
     */
    @Suppress("DEPRECATION")
    fun stop(dialogFragment: android.app.DialogFragment) {
        stop(dialogFragment.dialog)
    }

    /**
     * Stops logging touch events in the specified [popupWindow]. Calling this method before first
     * calling [stop] with the same popup window will have no effect.
     *
     * Note: If the [popupWindow] already has an [android.view.View.OnTouchListener] instance
     * connected, it will be overridden by an empty listener.
     */
    fun stop(popupWindow: PopupWindow) {
        popupWindow.setTouchInterceptor { _, _ -> false }
    }

    private fun stop(window: Window) {
        val callback = window.callback

        if (callback is TouchEventTracker) {
            window.callback = callback.callback
        }
    }

    /**
     * Processes the [motionEvent] to create a touch event.
     *
     * This method is called automatically when processing events after calling [start]. If, for
     * some reason, you need to process touch events manually (such as for an
     * [android.view.View.OnTouchListener] instance passed to
     * [android.widget.PopupWindow.setTouchInterceptor]), explicitly call
     * [com.behametrics.logger.utils.logging.processTouchEvent].
     */
    fun processTouchEvent(motionEvent: MotionEvent?) {
        if (!isRunning || motionEvent == null) {
            return
        }

        when (val action = motionEvent.actionMasked) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_POINTER_DOWN -> processTouchDown(motionEvent, action)
            MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> processTouchUp(motionEvent, action)
            MotionEvent.ACTION_MOVE -> processTouchMove(motionEvent, action)
            MotionEvent.ACTION_CANCEL -> processTouchCancel(motionEvent, action)
            else -> processTouchOther(motionEvent, action)
        }
    }

    private fun processTouchDown(motionEvent: MotionEvent, action: Int) {
        addTouchEvent(motionEvent, TOUCH_DOWN, action, motionEvent.actionIndex)
    }

    private fun processTouchUp(motionEvent: MotionEvent, action: Int) {
        addTouchEvent(motionEvent, TOUCH_UP, action, motionEvent.actionIndex)
    }

    private fun processTouchMove(motionEvent: MotionEvent, action: Int) {
        for (pointerIndex in 0 until motionEvent.pointerCount) {
            try {
                addTouchEvent(motionEvent, TOUCH_MOVE, action, pointerIndex)
            } catch (e: IndexOutOfBoundsException) {
            }
        }
    }

    private fun processTouchCancel(motionEvent: MotionEvent, action: Int) {
        addTouchEvent(motionEvent, TOUCH_CANCEL, action, motionEvent.actionIndex)
    }

    private fun processTouchOther(motionEvent: MotionEvent, action: Int) {
        addTouchEvent(motionEvent, TOUCH_OTHER, action, motionEvent.actionIndex)
    }

    private fun addTouchEvent(motionEvent: MotionEvent, eventType: String, actionType: Int, pointerIndex: Int) {
        inputEventDispatcher.dispatch(
            TouchEvent(
                sessionManager.sessionId,
                getNonSensorEventTimestampNanos(motionEvent.eventTime),
                eventType,
                motionEventActionMap.getName(actionType) ?: "",
                motionEvent.getPointerId(pointerIndex),
                motionEvent.getX(pointerIndex),
                motionEvent.getY(pointerIndex),
                motionEvent.getPressure(pointerIndex),
                motionEvent.getSize(pointerIndex),
                motionEvent.getTouchMajor(pointerIndex),
                motionEvent.getTouchMinor(pointerIndex),
                getRawX(motionEvent, pointerIndex),
                getRawY(motionEvent, pointerIndex)
            )
        )
    }

    private fun getRawX(motionEvent: MotionEvent, pointerIndex: Int): Float {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            motionEvent.getRawX(pointerIndex)
        } else {
            motionEvent.rawX
        }
    }

    private fun getRawY(motionEvent: MotionEvent, pointerIndex: Int): Float {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            motionEvent.getRawY(pointerIndex)
        } else {
            motionEvent.rawY
        }
    }
}
