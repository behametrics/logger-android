package com.behametrics.logger.config

import android.content.Context
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.File

private val internalStorageDirPath = "/path/to/internal"
private val externalStorageDirPaths = arrayOf(
    "/path/to/external_0", "/path/to/external_1", "/path/to/external_2", "/path/to/external_3")
private val primaryExternalStorageDirPath = externalStorageDirPaths[0]

internal class StorageDirPathManagerTest {

    @MockK
    private lateinit var mockedContext: Context
    @MockK
    private lateinit var mockedExternalStorageFile: File

    private var externalStorageFiles: Array<File>

    private lateinit var storageDirPathManager: StorageDirPathManager

    init {
        MockKAnnotations.init(this)

        every { mockedContext.filesDir.absolutePath } returns internalStorageDirPath

        externalStorageFiles = Array(externalStorageDirPaths.size) { mockk<File>() }
    }

    @BeforeEach
    fun setUp() {
        storageDirPathManager = StorageDirPathManager()
    }

    @ParameterizedTest
    @MethodSource("getStorageDirPathParams")
    fun getStorageDirPath(storageType: String, expectedStorageDirPath: String) {
        every { mockedContext.getExternalFilesDir(null) } returns mockedExternalStorageFile
        every { mockedExternalStorageFile.absolutePath } returns primaryExternalStorageDirPath

        every { mockedContext.getExternalFilesDirs(null) } returns externalStorageFiles
        for (i in 0 until externalStorageDirPaths.size) {
            every { externalStorageFiles[i].absolutePath } returns externalStorageDirPaths[i]
        }

        assertEquals(expectedStorageDirPath, storageDirPathManager.getStorageDirPath(mockedContext, storageType))
    }

    @ParameterizedTest
    @MethodSource("getStorageDirPath_ReturnInternalIfExternalNotAvailableParams")
    fun getStorageDirPath_ReturnInternalIfExternalNotAvailable(storageType: String) {
        every { mockedContext.getExternalFilesDir(null) } returns null
        every { mockedContext.getExternalFilesDirs(null) } returns null

        assertEquals(internalStorageDirPath, storageDirPathManager.getStorageDirPath(mockedContext, storageType))
    }

    companion object {
        @JvmStatic
        fun getStorageDirPathParams() = arrayOf<Any?>(
            arrayOf<Any?>("internal", internalStorageDirPath),
            arrayOf<Any?>("", internalStorageDirPath),
            arrayOf<Any?>("invalid_type", internalStorageDirPath),
            arrayOf<Any?>("external", primaryExternalStorageDirPath),
            arrayOf<Any?>("external_", internalStorageDirPath),
            arrayOf<Any?>("external_0", primaryExternalStorageDirPath),
            arrayOf<Any?>("external_1", externalStorageDirPaths[1]),
            arrayOf<Any?>("external_2", externalStorageDirPaths[2]),
            arrayOf<Any?>("external_3", externalStorageDirPaths[3]),
            arrayOf<Any?>("external_4", primaryExternalStorageDirPath),
            arrayOf<Any?>("external_-1", primaryExternalStorageDirPath),
            arrayOf<Any?>("external_invalid_number_format", internalStorageDirPath)
        )

        @JvmStatic
        fun getStorageDirPath_ReturnInternalIfExternalNotAvailableParams() = arrayOf<Any?>(
            arrayOf<Any?>("internal"),
            arrayOf<Any?>("external"),
            arrayOf<Any?>("external_1"),
            arrayOf<Any?>("external_4")
        )
    }
}
