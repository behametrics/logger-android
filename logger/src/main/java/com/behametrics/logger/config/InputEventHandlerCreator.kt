package com.behametrics.logger.config

import android.content.Context
import com.behametrics.logger.handlers.CsvFileHandler
import com.behametrics.logger.handlers.DebugLogOutputHandler
import com.behametrics.logger.handlers.InputEventHandler
import com.behametrics.logger.handlers.JsonNetworkHandler

/**
 * Class providing the creation of [InputEventHandler] instances from the supplied [config]. See the
 * `Outputs` entry in the `assets/config.properties` file for possible values.
 *
 * If some entries in the [config] are invalid, values from the [DefaultConfig] is used.
 */
class InputEventHandlerCreator(private val config: Config) {

    /** Creates input event handlers given the supplied [context] and [config]. */
    fun createHandlers(context: Context): MutableCollection<InputEventHandler> {
        val handlers = mutableListOf<InputEventHandler>()

        for (handlerName in config.getStringArrayEntry("Outputs").distinct()) {
            when (handlerName) {
                "local" -> handlers.add(
                    CsvFileHandler(
                        getLogDirPath(
                            context,
                            config.getStringEntry("StorageType"),
                            config.getStringEntry("LogsDirPath")
                        ),
                        config.getIntEntry(
                            "MinFreeSpaceLimitMB", DefaultConfig.getIntEntry("MinFreeSpaceLimitMB")
                        )
                    )
                )
                "server" -> handlers.add(
                    JsonNetworkHandler(
                        config.getStringEntry("ServerUrl"),
                        config.getStringEntry("EndpointUrl"),
                        config.getStringEntry("LoggerHeader")
                    )
                )
                "debug" -> handlers.add(DebugLogOutputHandler())
            }
        }

        return handlers
    }

    private fun getLogDirPath(context: Context, storageType: String, relativeDirPath: String): String {
        return StorageDirPathManager().getStorageDirPath(context, storageType) + "/" + relativeDirPath
    }
}
