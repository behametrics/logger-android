package com.behametrics.logger.handlers

import android.util.Log
import com.behametrics.logger.inputevents.InputEvent
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.concurrent.TimeUnit

private const val BYTES_PER_MB: Int = 1024 * 1024

/**
 * Class writing input events to files in the comma-separated values (CSV) format.
 *
 * Each input is written to a separate file. If the input event type has a header defined, the
 * header is written to the file in the first line.
 *
 * @param logDirPath          directory path to write CSV files to
 * @param minFreeSpaceLimitMB minimum required free storage space in megabytes
 * @param fieldSeparator      separator between each field
 */
class CsvFileHandler @JvmOverloads constructor(
    private val logDirPath: String,
    private val minFreeSpaceLimitMB: Int,
    private val fieldSeparator: String = ",",
    inputEventBatchSize: Int = 100,
    inputEventMaxHandlingDelayMillis: Long = 500
) : InputEventHandler {

    private val bufferedWriters: LinkedHashMap<String, BufferedWriter> = LinkedHashMap()

    private val subject = BehaviorSubject.create<InputEvent>()
    private var disposable: Disposable? = null

    init {
        disposable = subject
            .subscribeOn(Schedulers.io())
            .buffer(inputEventMaxHandlingDelayMillis, TimeUnit.MILLISECONDS, inputEventBatchSize)
            .subscribeBy(
                onNext = { it.forEach { inputEvent -> write(inputEvent) } },
                onError = { it.printStackTrace() }
            )
    }

    override fun handle(inputEvent: InputEvent) {
        try {
            subject.onNext(inputEvent)
        } catch (e: IOException) {
            Log.e("CsvFileHandler", "Could not write event: $e")
        } catch (e: OutOfMemoryError) {
            Log.e("CsvFileHandler", "Could not write event: $e")
        }
    }

    private fun write(inputEvent: InputEvent) {
        if (!bufferedWriters.containsKey(inputEvent.inputName)) {
            createLogFile(inputEvent.inputName, inputEvent.getInputHeader(fieldSeparator))
        }

        val bufferedWriter = bufferedWriters[inputEvent.inputName]
        if (bufferedWriter != null) {
            bufferedWriter.write(inputEvent.toCsv(fieldSeparator))
            bufferedWriter.flush()
        }
    }

    private fun createLogFile(filename: String, header: String? = null) {
        createLogsDir()

        val logFilePath = "$logDirPath/$filename.csv"
        val shouldWriteHeader = !fileExists(logFilePath)
        val bufferedWriter: BufferedWriter

        if (getUsableSpaceMB() > minFreeSpaceLimitMB) {
            bufferedWriter = getLogFile(logFilePath)
        } else {
            throw OutOfMemoryError("Free space limit is less than " + minFreeSpaceLimitMB + "MB")
        }

        if (shouldWriteHeader && header != null) {
            bufferedWriter.write(header + "\n")
            bufferedWriter.flush()
        }

        bufferedWriters[filename] = bufferedWriter
    }

    private fun getUsableSpaceMB(): Long {
        return File(logDirPath).usableSpace / BYTES_PER_MB
    }

    private fun createLogsDir(): Boolean {
        return File(logDirPath).mkdirs()
    }

    @Throws(IOException::class)
    private fun getLogFile(logFilePath: String): BufferedWriter {
        return BufferedWriter(FileWriter(logFilePath, true))
    }

    private fun fileExists(logFilePath: String): Boolean {
        return File(logFilePath).exists()
    }
}
