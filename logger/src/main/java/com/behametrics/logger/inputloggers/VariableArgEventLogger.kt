package com.behametrics.logger.inputloggers

import android.content.Context
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.VariableArgEvent
import com.behametrics.logger.utils.session.SessionManager
import com.behametrics.logger.utils.getNonSensorEventTimestampNanos

/**
 * Class providing logging of events with a variable number of arguments. This class allows you to
 * log custom events under the specified [inputName].
 *
 * [scope] is set to [InputLogger.Scopes.DEVICE] by default.
 *
 * @see VariableArgEvent
 */
open class VariableArgEventLogger @JvmOverloads constructor(
    inputEventDispatcher: InputEventDispatcher,
    sessionManager: SessionManager,
    val inputName: String,
    private val header: Array<String>? = null,
    scope: Scopes = Scopes.DEVICE
) : InputLogger(inputEventDispatcher, sessionManager, scope) {

    private var loggingEnabled = false

    companion object {
        const val UNKNOWN_EVENT_FIELD_PREFIX = "value"
    }

    override fun startLogging(context: Context) {
        loggingEnabled = true
    }

    override fun stopLogging(context: Context) {
        loggingEnabled = false
    }

    /**
     * Logs an event if logging is enabled (by calling [start]). The [eventData] represent the event
     * contents.
     */
    fun log(vararg eventData: String) {
        if (!loggingEnabled) {
            return
        }

        inputEventDispatcher.dispatch(
            VariableArgEvent(
                inputName,
                sessionManager.sessionId,
                getNonSensorEventTimestampNanos(),
                eventData,
                header
            )
        )
    }
}
