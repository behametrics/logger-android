package com.behametrics.logger.inputloggers

import android.app.Activity
import android.content.Context
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.utils.session.SessionManager
import com.behametrics.logger.inputevents.InputEvent
import com.behametrics.logger.utils.session.SessionEvent
import com.behametrics.logger.utils.session.SessionEventListener
import java.lang.IllegalArgumentException
import java.util.*

/**
 * Abstract class for input loggers yielding events.
 *
 * The [inputEventDispatcher] is responsible for dispatching input events to available handlers. The
 * [sessionManager] provides input events with the current session ID.
 *
 * [scope] indicates when the logger should be running:
 * * [Scopes.APPLICATION] - logger is running while the application is in the foreground. This is
 *   the default unless specified otherwise in subclasses.
 * * [Scopes.DEVICE] - logger is running while the application is running, even in the background or
 *   when the display is turned off.
 * * [Scopes.DEVICE_DISPLAY_ON] - logger is running while the application is running in the
 *   foreground or background, as long as the device display is turned on.
 *
 * The [scope] also determines when the logger stops. If [stop] is called and the [scope] is
 * [Scopes.APPLICATION], then the logger is stopped only if there is no activity currently in the
 * resumed state. This ensures compatibility with Android Q and above which allows multiple
 * activities being in the resumed state at once.
 *
 * Setting the [scope] will take effect only if [start] is called and the logger is stopped prior to
 * starting.
 *
 * @see InputEvent
 */
abstract class InputLogger(
    val inputEventDispatcher: InputEventDispatcher,
    protected val sessionManager: SessionManager,
    open var scope: Scopes = Scopes.APPLICATION
) : SessionEventListener {

    /** Returns true if the logger is running, false otherwise. */
    var isRunning = false
        private set

    /**
     * The type of context used to start and stop logging. Applies only if [scope] is
     * [Scopes.APPLICATION]. Subclasses may override this value to modify the behavior of [start]
     * and [stop].
     */
    open val loggingContext = LoggingContext.APPLICATION

    internal val activitiesUnderLogging = mutableSetOf<Activity>()
    internal var currentScope = Scopes.APPLICATION

    /**
     * Starts logging events using the specified [context].
     *
     * If [scope] is [Scopes.APPLICATION], an [Activity] instance must be passed, otherwise
     * [IllegalArgumentException] is thrown.
     *
     * If [scope] is [Scopes.APPLICATION] and [loggingContext] is [LoggingContext.ACTIVITY], logging
     * is started in each activity individually.
     *
     * If [scope] is [Scopes.APPLICATION] and [loggingContext] is [LoggingContext.APPLICATION],
     * logging is started in the entire application. Multiple calls to this method with different
     * activities affects when [stop] stops logging in the application.
     *
     * For other values of [scope]:
     * * Logging is started in the entire application and multiple calls to this method with any
     *   activity without first calling [stop] has no effect.
     * * The application context is used to start logging (via [Context.getApplicationContext]) as
     *   other scopes may span beyond the application.
     *
     * @see stop
     */
    @Synchronized
    fun start(context: Context) {
        setCurrentScope()

        if (currentScope == Scopes.APPLICATION) {
            if (context is Activity) {
                startForApplicationScope(context)
            } else {
                throw IllegalArgumentException("context must be an Activity instance for scope ${Scopes.APPLICATION}")
            }
        } else {
            startForNonApplicationScope(context.applicationContext)
        }
    }

    private fun setCurrentScope() {
        if (!isRunning) {
            currentScope = scope
        }
    }

    private fun startForApplicationScope(activity: Activity) {
        if (loggingContext == LoggingContext.APPLICATION) {
            if (!isRunning) {
                isRunning = true
                doStartLogging(activity)
            }

            activitiesUnderLogging.add(activity)
        } else if (loggingContext == LoggingContext.ACTIVITY) {
            if (!activitiesUnderLogging.contains(activity)) {
                activitiesUnderLogging.add(activity)

                isRunning = true
                doStartLogging(activity)
            }
        }
    }

    private fun startForNonApplicationScope(context: Context) {
        if (!isRunning) {
            isRunning = true
            doStartLogging(context)
        }
    }

    private fun doStartLogging(context: Context) {
        startLogging(context)
        sessionManager.registerListener(this)
    }

    /**
     * Stops logging events using the specified [context].
     *
     * If [scope] is [Scopes.APPLICATION], an [Activity] instance must be passed, otherwise
     * [IllegalArgumentException] is thrown.
     *
     * If [scope] is [Scopes.APPLICATION] and [loggingContext] is [LoggingContext.ACTIVITY], logging
     * is stopped only in the activity associated with the [context].
     *
     * If [scope] is [Scopes.APPLICATION] and [loggingContext] is [LoggingContext.APPLICATION],
     * logging is stopped only if there are no more activities that were being logged by calling
     * [start] on them.
     *
     * For other values of [scope], logging is stopped unconditionally in the entire application.
     *
     * Calling this method before first calling [start] has no effect.
     *
     * @see start
     */
    @Synchronized
    fun stop(context: Context) {
        if (currentScope == Scopes.APPLICATION) {
            if (context is Activity) {
                stopForApplicationScope(context)
            } else {
                throw IllegalArgumentException("context must be an Activity instance for scope ${Scopes.APPLICATION}")
            }
        } else {
            stopForNonApplicationScope(context.applicationContext)
        }
    }

    private fun stopForApplicationScope(activity: Activity) {
        if (loggingContext == LoggingContext.APPLICATION) {
            activitiesUnderLogging.remove(activity)

            if (isRunning && activitiesUnderLogging.isEmpty()) {
                isRunning = false
                doStopLogging(activity)
            }
        } else if (loggingContext == LoggingContext.ACTIVITY) {
            if (activitiesUnderLogging.contains(activity)) {
                activitiesUnderLogging.remove(activity)
                doStopLogging(activity)
            }

            if (activitiesUnderLogging.isEmpty()) {
                isRunning = false
            }
        }
    }

    private fun stopForNonApplicationScope(context: Context) {
        if (isRunning) {
            isRunning = false
            doStopLogging(context)
        }
    }

    /**
     * Stops logging events using the specified [context].
     *
     * If [forceStop] is true, logging is stopped unconditionally in the entire application
     * regardless of the [scope] and no exception is thrown.
     */
    @Synchronized
    fun stop(context: Context, forceStop: Boolean) {
        if (forceStop) {
            if (currentScope == Scopes.APPLICATION) {
                forceStopForApplicationScope(context)
            } else {
                stop(context)
            }
        } else {
            stop(context)
        }
    }

    private fun forceStopForApplicationScope(context: Context) {
        if (loggingContext == LoggingContext.APPLICATION) {
            activitiesUnderLogging.clear()

            if (isRunning) {
                isRunning = false
                doStopLogging(context)
            }
        } else if (loggingContext == LoggingContext.ACTIVITY) {
            activitiesUnderLogging.forEach {
                doStopLogging(it)
            }
            activitiesUnderLogging.clear()

            isRunning = false
        }
    }

    private fun doStopLogging(context: Context) {
        stopLogging(context)
        sessionManager.unregisterListener(this)
    }

    /**
     * Sets the logger scope given the [scopeName]. The [scopeName] must match one of the values in
     * [Scopes]. If there is no such match, this method has no effect. The matching is
     * case-insensitive.
     *
     * Setting the scope will take effect only if [start] is called and the logger is stopped prior
     * to starting.
     */
    fun setScope(scopeName: String) {
        try {
            scope = Scopes.valueOf(scopeName.toUpperCase(Locale.US))
        } catch (e: IllegalArgumentException) {
        }
    }

    /**
     * Processes the change in session ID generated by [sessionManager].
     *
     * This method by default does nothing. Subclasses may override this method to react to changes
     * in session ID.
     */
    override fun onSessionChanged(event: SessionEvent) {
    }

    protected abstract fun startLogging(context: Context)

    protected abstract fun stopLogging(context: Context)

    /**
     * Indicates the scope in which the logger should perform logging.
     */
    enum class Scopes {
        DEVICE, DEVICE_DISPLAY_ON, APPLICATION
    }

    /**
     * Indicates the type of context that is used to start and stop logging. [APPLICATION] refers to
     * the context returned by [Context.getApplicationContext], [ACTIVITY] refers to the activity
     * itself (since an activity is also a [Context]).
     */
    enum class LoggingContext {
        APPLICATION, ACTIVITY
    }
}
