package com.behametrics.logger.inputloggers

import android.app.Activity
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.utils.session.SessionManager
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class SensorLoggerTest {

    @MockK
    private lateinit var mockedActivity: Activity
    @MockK
    private lateinit var mockedApplicationContext: Context
    @RelaxedMockK
    private lateinit var mockedSensorManager: SensorManager
    @RelaxedMockK
    private lateinit var mockedSensor: Sensor
    @RelaxedMockK
    private lateinit var mockedInputEventDispatcher: InputEventDispatcher
    @RelaxedMockK
    private lateinit var mockedSessionManager: SessionManager

    private lateinit var sensorLogger: SensorLogger

    private var allSensors: List<Sensor>
    private val allSensorsSize = 10

    init {
        MockKAnnotations.init(this)

        allSensors = List(allSensorsSize) { mockedSensor }

        every { mockedActivity.applicationContext } returns mockedApplicationContext
        every { mockedApplicationContext.getSystemService(any()) } returns mockedSensorManager
        every { mockedSensorManager.getSensorList(any()) } returns allSensors
    }

    @BeforeEach
    fun setUp() {
        sensorLogger = SensorLogger(mockedInputEventDispatcher, mockedSessionManager)
    }

    @ParameterizedTest
    @MethodSource("startAllSensorsParams")
    fun startAllSensors(sensorNames: Iterable<String>) {
        sensorLogger.setSensors(sensorNames)
        sensorLogger.start(mockedActivity)

        verify(exactly = 1) {
            mockedSensorManager.getSensorList(any())
        }

        verify(exactly = allSensorsSize) {
            mockedSensorManager.registerListener(any(), any<Sensor>(), any())
        }
    }

    @ParameterizedTest
    @MethodSource("startSpecificSensorsParams")
    fun startSpecificSensors(sensorNames: Iterable<String>, numValidSensors: Int) {
        sensorLogger.setSensors(sensorNames)
        sensorLogger.start(mockedActivity)

        verify(exactly = 0) {
            mockedSensorManager.getSensorList(any())
        }

        verify(exactly = numValidSensors) {
            mockedSensorManager.registerListener(any(), any<Sensor>(), any())
        }
    }

    @ParameterizedTest
    @MethodSource("setDelayStringParams")
    fun setDelayString(delayName: String, expectedDelay: Int) {
        sensorLogger.setDelay(delayName)
        assertEquals(expectedDelay, sensorLogger.delay)
    }

    companion object {
        @JvmStatic
        fun startAllSensorsParams() = arrayOf<Any?>(
            arrayOf<Any?>(listOf("all")),
            arrayOf<Any?>(listOf("accelerometer", "all")),
            arrayOf<Any?>(listOf("all", "accelerometer"))
        )

        @JvmStatic
        fun startSpecificSensorsParams() = arrayOf<Any?>(
            arrayOf<Any?>(listOf("accelerometer"), 1),
            arrayOf<Any?>(listOf("accelerometer", "gyroscope"), 2),
            arrayOf<Any?>(listOf(""), 0),
            arrayOf<Any?>(listOf("accelerometer", "invalid_sensor"), 1),
            arrayOf<Any?>(listOf("invalid_sensor", "invalid_sensor2"), 0)
        )

        @JvmStatic
        fun setDelayStringParams() = arrayOf<Any?>(
            arrayOf<Any?>("normal", SensorManager.SENSOR_DELAY_NORMAL),
            arrayOf<Any?>("invalid", SensorLogger.DEFAULT_SENSOR_DELAY),
            arrayOf<Any?>("", SensorLogger.DEFAULT_SENSOR_DELAY)
        )
    }
}
