package com.behametrics.demoapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.behametrics.demoapp.R

import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        buttonShowAnotherFragment.setOnClickListener {
            childFragmentManager.beginTransaction().add(R.id.listFragmentLayout, NestedFragment()).commit()
        }
    }
}
