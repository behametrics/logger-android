package com.behametrics.logger

import android.app.Activity
import android.os.Bundle
import com.behametrics.logger.inputloggers.InputLogger
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
internal class LoggerTest {

    private val inputLoggers = arrayOf(
        Logger.inputLoggerManager.touchLogger,
        Logger.inputLoggerManager.sensorLogger,
        Logger.inputLoggerManager.orientationLogger,
        Logger.inputLoggerManager.lifecycleLogger,
        Logger.inputLoggerManager.customEventLogger
    )

    @Test
    fun startBeforeCreate_WithoutStartAll() {
        startAndStopLogging(
            TestActivity.States.BEFORE_CREATE,
            null,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(false, false, true, true, false).toBooleanArray()
            ),
            false
        )
    }

    @Test
    fun startBeforeCreate_WithStartAll() {
        startAndStopLogging(
            TestActivity.States.BEFORE_CREATE,
            null,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(false, false, true, true, false).toBooleanArray()
            )
        )
    }

    @Test
    fun startAfterCreate() {
        startAndStopLogging(
            TestActivity.States.AFTER_CREATE,
            null,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(false, false, true, true, false).toBooleanArray()
            )
        )
    }

    @Test
    fun startAfterResume() {
        startAndStopLogging(
            TestActivity.States.AFTER_RESUME,
            null,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(false, false, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(false, false, true, true, false).toBooleanArray()
            )
        )
    }

    @Test
    fun startAfterPause() {
        startAndStopLogging(
            TestActivity.States.AFTER_PAUSE,
            null,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(true, true, true, true, false).toBooleanArray()
            )
        )
    }

    @Test
    fun startStopInOneActivity() {
        startAndStopLogging(
            TestActivity.States.AFTER_RESUME,
            TestActivity.States.AFTER_PAUSE,
            mapOf(
                TestActivity.States.INITIALIZED to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_CREATE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.BEFORE_RESUME to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_RESUME to arrayOf(true, true, true, true, false).toBooleanArray(),
                TestActivity.States.AFTER_PAUSE to arrayOf(false, false, false, false, false).toBooleanArray(),
                TestActivity.States.AFTER_DESTROY to arrayOf(false, false, false, false, false).toBooleanArray()
            )
        )
    }

    private fun startAndStopLogging(
        loggerStartAtState: TestActivity.States?,
        loggerStopAtState: TestActivity.States?,
        expectedIsRunningPerState: Map<TestActivity.States, BooleanArray>,
        startAll: Boolean = true
    ) {
        val controller = Robolectric.buildActivity(TestActivity::class.java)
        val activity = controller.get()

        activity.startAll = startAll
        activity.loggerStartAtState = loggerStartAtState
        activity.loggerStopAtState = loggerStopAtState
        activity.inputLoggers = inputLoggers

        controller.create().start().resume().pause().stop().destroy()

        Logger.stop(activity)

        for (state in expectedIsRunningPerState.keys) {
            assertArrayEquals(expectedIsRunningPerState[state], activity.runningLoggersPerState[state])
        }
    }

    internal class TestActivity : Activity() {

        var startAll = true
        var loggerStartAtState: States? = null
        var loggerStopAtState: States? = null
        val runningLoggersPerState = mutableMapOf<States, BooleanArray>()
        var inputLoggers = arrayOf<InputLogger>()

        enum class States {
            INITIALIZED,
            BEFORE_CREATE,
            AFTER_CREATE,
            BEFORE_RESUME,
            AFTER_RESUME,
            BEFORE_PAUSE,
            AFTER_PAUSE,
            BEFORE_DESTROY,
            AFTER_DESTROY
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            runningLoggersPerState[States.INITIALIZED] = getIsRunningForLoggers()

            startStopLoggerAtState(States.BEFORE_CREATE)
            super.onCreate(savedInstanceState)
            startStopLoggerAtState(States.AFTER_CREATE)
        }

        override fun onResume() {
            startStopLoggerAtState(States.BEFORE_RESUME)
            super.onResume()
            startStopLoggerAtState(States.AFTER_RESUME)
        }

        override fun onPause() {
            startStopLoggerAtState(States.BEFORE_PAUSE)
            super.onPause()
            startStopLoggerAtState(States.AFTER_PAUSE)
        }

        override fun onDestroy() {
            startStopLoggerAtState(States.BEFORE_DESTROY)
            super.onDestroy()
            startStopLoggerAtState(States.AFTER_DESTROY)
        }

        private fun startStopLoggerAtState(state: States) {
            if (loggerStartAtState == state) {
                Logger.start(this, startAll)
            }
            if (loggerStopAtState == state) {
                Logger.stop(this)
            }
            runningLoggersPerState[state] = getIsRunningForLoggers()
        }

        private fun getIsRunningForLoggers(): BooleanArray {
            return inputLoggers.map { it.isRunning }.toBooleanArray()
        }
    }
}
