package com.behametrics.logger.utils.maps.initializers

import com.behametrics.logger.utils.maps.MemberNameValueMap
import java.lang.reflect.Field

/**
 * Class initializing [MemberNameValueMap] instances with static class members matching the [prefix]
 * (e.g. static members of [android.hardware.Sensor] whose names start with `TYPE_*`.
 *
 * If one or more members share the same value, the member name processed last will be mapped to the
 * value.
 */
class PrefixBasedMapInitializer<T>(private val prefix: String) : MapInitializer<T> {

    override fun initMap(map: MemberNameValueMap<T>, classWithFields: Class<*>) {
        for (field in classWithFields.fields) {
            if (field.name.startsWith(prefix)) {
                val fieldName = getFieldName(map, field)
                val fieldValue = map.getStaticMemberValue(field)

                if (fieldValue != null) {
                    map.put(fieldName, fieldValue)
                }
            }
        }
    }

    private fun getFieldName(map: MemberNameValueMap<T>, field: Field): String {
        return map.getProcessedName(field.name.substring(prefix.length))
    }
}
