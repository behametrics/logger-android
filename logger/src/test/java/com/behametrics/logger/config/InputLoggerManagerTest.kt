package com.behametrics.logger.config

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputloggers.*
import com.behametrics.logger.utils.session.SessionManager
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.reflect.KClass

internal class InputLoggerManagerTest {

    @RelaxedMockK
    private lateinit var mockedInputEventDispatcher: InputEventDispatcher
    @RelaxedMockK
    private lateinit var mockedSessionManager: SessionManager
    @RelaxedMockK
    private lateinit var mockedActivity: AppCompatActivity
    @RelaxedMockK
    private lateinit var secondMockedActivity: AppCompatActivity
    @RelaxedMockK
    private lateinit var mockedContext: Context
    @RelaxedMockK
    private lateinit var mockedFragmentManager: FragmentManager

    private lateinit var inputLoggerManager: InputLoggerManager

    init {
        MockKAnnotations.init(this)

        every { mockedActivity.supportFragmentManager } returns mockedFragmentManager
        every { secondMockedActivity.supportFragmentManager } returns mockedFragmentManager
    }

    @BeforeEach
    fun setUp() {
        inputLoggerManager = spyk(InputLoggerManager(mockedInputEventDispatcher, mockedSessionManager))
    }

    @ParameterizedTest
    @MethodSource("startAndStopLogging_RegisterAndUnregisterForTheSameActivityIsCalledOnceParams")
    fun startAndStopLogging_RegisterAndUnregisterForTheSameActivityIsCalledOnce(
        numStartCalls: Int,
        numStopCalls: Int,
        expectedNumRegisterCalls: Int,
        expectedNumUnregisterCalls: Int
    ) {
        repeat(numStartCalls) {
            inputLoggerManager.startLogging(mockedActivity)
        }

        repeat(numStopCalls) {
            inputLoggerManager.stopLogging(mockedActivity)
        }

        verify(exactly = expectedNumRegisterCalls) {
            mockedFragmentManager.registerFragmentLifecycleCallbacks(any(), any())
        }
        verify(exactly = expectedNumUnregisterCalls) {
            mockedFragmentManager.unregisterFragmentLifecycleCallbacks(any())
        }
    }

    @Test
    fun startAndStopLogging_RegisterAndUnregisterForMultipleActivities() {
        inputLoggerManager.startLogging(mockedActivity)
        inputLoggerManager.startLogging(secondMockedActivity)

        inputLoggerManager.stopLogging(mockedActivity)
        inputLoggerManager.stopLogging(secondMockedActivity)

        verify(exactly = 2) {
            mockedFragmentManager.registerFragmentLifecycleCallbacks(any(), any())
        }
        verify(exactly = 2) {
            mockedFragmentManager.unregisterFragmentLifecycleCallbacks(any())
        }
    }

    @Test
    fun startAndStopLogging_RegisteringAndUnregisteringIsNotCalledForNonActivityContexts() {
        inputLoggerManager.startLogging(mockedContext)
        inputLoggerManager.stopLogging(mockedContext)

        verify(exactly = 0) {
            mockedFragmentManager.registerFragmentLifecycleCallbacks(any(), any())
        }
        verify(exactly = 0) {
            mockedFragmentManager.unregisterFragmentLifecycleCallbacks(any())
        }
    }

    @Test
    fun startAndStopLogging_SettingInputsWhileRunningDoesNotAffectStop() {
        inputLoggerManager.setInputs(arrayOf("touch", "orientation"))
        inputLoggerManager.startLogging(mockedActivity)
        inputLoggerManager.setInputs(arrayOf("lifecycle", "custom"))
        inputLoggerManager.stopLogging(mockedActivity)

        assertFalse(inputLoggerManager.touchLogger.isRunning)
        assertFalse(inputLoggerManager.orientationLogger.isRunning)
        assertFalse(inputLoggerManager.customEventLogger.isRunning)
        assertFalse(inputLoggerManager.sensorLogger.isRunning)
        assertFalse(inputLoggerManager.lifecycleLogger.isRunning)
        assertFalse(inputLoggerManager.keyboardLogger.isRunning)
    }

    @Test
    fun startAndStopLogging_SettingInputsBetweenMultipleStartsDoesNotAffectStop() {
        inputLoggerManager.setInputs(arrayOf("touch", "orientation"))
        inputLoggerManager.startLogging(mockedActivity)
        inputLoggerManager.setInputs(arrayOf("lifecycle", "custom"))
        inputLoggerManager.startLogging(mockedActivity)

        assertTrue(inputLoggerManager.touchLogger.isRunning)
        assertTrue(inputLoggerManager.orientationLogger.isRunning)
        assertFalse(inputLoggerManager.customEventLogger.isRunning)
        assertFalse(inputLoggerManager.sensorLogger.isRunning)
        assertFalse(inputLoggerManager.lifecycleLogger.isRunning)
        assertFalse(inputLoggerManager.keyboardLogger.isRunning)

        inputLoggerManager.stopLogging(mockedActivity)

        assertFalse(inputLoggerManager.touchLogger.isRunning)
        assertFalse(inputLoggerManager.orientationLogger.isRunning)
        assertFalse(inputLoggerManager.customEventLogger.isRunning)
        assertFalse(inputLoggerManager.sensorLogger.isRunning)
        assertFalse(inputLoggerManager.lifecycleLogger.isRunning)
        assertFalse(inputLoggerManager.keyboardLogger.isRunning)
    }

    @Test
    fun getInputLoggerSubset_All() {
        testGetInputLoggerSubset(
            inputLoggerManager,
            null,
            listOf(inputLoggerManager.touchLogger, inputLoggerManager.orientationLogger, inputLoggerManager.sensorLogger),
            listOf(inputLoggerManager.touchLogger, inputLoggerManager.orientationLogger, inputLoggerManager.sensorLogger))
    }

    @Test
    fun getInputLoggerSubset_NonNullPredicate() {
        testGetInputLoggerSubset(
            inputLoggerManager,
            { it.scope == InputLogger.Scopes.DEVICE },
            listOf(inputLoggerManager.touchLogger, inputLoggerManager.orientationLogger, inputLoggerManager.sensorLogger),
            listOf(inputLoggerManager.orientationLogger))
    }

    private fun testGetInputLoggerSubset(
        inputLoggerManager: InputLoggerManager,
        subsetPredicate: SubsetPredicate,
        inputLoggers: Collection<InputLogger>,
        expectedInputLoggers: Collection<InputLogger>
    ) {
        assertEquals(expectedInputLoggers, inputLoggerManager.getInputLoggerSubset(inputLoggers, subsetPredicate))
    }

    @ParameterizedTest
    @MethodSource("getInputLoggersParams")
    fun getInputLoggers(inputs: Array<String>, expectedInputLoggerClasses: Array<KClass<out InputLogger>>) {
        val inputLoggers = inputLoggerManager.getInputLoggers(inputs)
        val actualInputLoggerClasses = inputLoggers.map { it::class }

        assertEquals(expectedInputLoggerClasses.size, actualInputLoggerClasses.size)
        assertEquals(expectedInputLoggerClasses.toSet(), actualInputLoggerClasses.toSet())
    }

    companion object {
        @JvmStatic
        fun startAndStopLogging_RegisterAndUnregisterForTheSameActivityIsCalledOnceParams() = arrayOf<Any?>(
            arrayOf<Any?>(1, 0, 1, 0),
            arrayOf<Any?>(2, 0, 1, 0),
            arrayOf<Any?>(1, 1, 1, 1),
            arrayOf<Any?>(0, 1, 0, 0),
            arrayOf<Any?>(0, 2, 0, 0),
            arrayOf<Any?>(1, 2, 1, 1)
        )

        @JvmStatic
        fun getInputLoggersParams() = arrayOf<Any?>(
            arrayOf<Any?>(arrayOf("touch"), arrayOf(TouchLogger::class)),
            arrayOf<Any?>(
                arrayOf("touch", "orientation", "lifecycle"),
                arrayOf(TouchLogger::class, OrientationLogger::class, LifecycleLogger::class)),
            arrayOf<Any?>(
                arrayOf("touch", "touch", "lifecycle"),
                arrayOf(TouchLogger::class, LifecycleLogger::class)),
            arrayOf<Any?>(arrayOf("all"), getAllInputLoggerTypes()),
            arrayOf<Any?>(arrayOf("touch", "all", "sensor_accelerometer"), getAllInputLoggerTypes()),
            arrayOf<Any?>(arrayOf("sensor_accelerometer"), arrayOf(SensorLogger::class)),
            arrayOf<Any?>(arrayOf("sensor_accelerometer", "sensor_gyroscope"), arrayOf(SensorLogger::class)),
            arrayOf<Any?>(arrayOf("sensor_invalid_name"), arrayOf(SensorLogger::class)),
            arrayOf<Any?>(arrayOf("sensor_"), arrayOf(SensorLogger::class)),
            arrayOf<Any?>(arrayOf("sensor"), arrayOf<KClass<out InputLogger>>()),
            arrayOf<Any?>(arrayOf(""), arrayOf<KClass<out InputLogger>>())
        )

        private fun getAllInputLoggerTypes(): Array<KClass<out InputLogger>> {
            return arrayOf(
                TouchLogger::class,
                OrientationLogger::class,
                VariableArgEventLogger::class,
                SensorLogger::class,
                LifecycleLogger::class,
                KeyboardLogger::class)
        }
    }
}
