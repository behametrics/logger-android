@file:JvmName("TimestampConverter")

package com.behametrics.logger.utils

import android.os.SystemClock
import android.view.MotionEvent

private val startTimeMillis: Long = System.currentTimeMillis() - SystemClock.uptimeMillis()
private const val NANOSECONDS_PER_MILLISECOND: Long = 1000000

/**
 * Returns a timestamp in nanoseconds ensuring consistency of timestamps for non-sensor events
 * (touch, orientation, custom events, ...) with sensor events (accelerometer, gyroscope, ...),
 * given the current [upTimeMillis].
 *
 * Usually, you can rely on the default parameter value for [upTimeMillis] unless an event provides
 * a specific timestamp (such as [MotionEvent.getEventTime]).
 *
 * Use this method for non-sensor events only as sensor event timestamps are recomputed differently.
 */
fun getNonSensorEventTimestampNanos(upTimeMillis: Long = SystemClock.uptimeMillis()): Long {
    return (startTimeMillis + upTimeMillis) * NANOSECONDS_PER_MILLISECOND
}
