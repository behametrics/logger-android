@file:JvmName("TouchLogging")

package com.behametrics.logger.utils.logging

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.DialogFragment
import android.widget.PopupWindow
import com.behametrics.logger.Logger
import com.behametrics.logger.inputloggers.TouchLogger

/**
 * Starts logging touch events in the dialog.
 *
 * @see [TouchLogger.start]
 */
fun Dialog.startTouchLogging() {
    Logger.inputLoggerManager.touchLogger.start(this)
}

/**
 * Stops logging touch events in the dialog.
 *
 * @see [TouchLogger.stop]
 */
fun Dialog.stopTouchLogging() {
    Logger.inputLoggerManager.touchLogger.stop(this)
}

/**
 * Starts logging touch events in the dialog fragment.
 *
 * @see [TouchLogger.start]
 */
fun DialogFragment.startTouchLogging() {
    Logger.inputLoggerManager.touchLogger.start(this)
}

/**
 * Stops logging touch events in the dialog fragment.
 *
 * @see [TouchLogger.stop]
 */
fun DialogFragment.stopTouchLogging() {
    Logger.inputLoggerManager.touchLogger.stop(this)
}

/**
 * Starts logging touch events in the dialog fragment.
 *
 * @see [TouchLogger.start]
 */
@Suppress("DEPRECATION")
fun android.app.DialogFragment.startTouchLogging() {
    Logger.inputLoggerManager.touchLogger.start(this)
}

/**
 * Stops logging touch events in the dialog fragment.
 *
 * @see [TouchLogger.stop]
 */
@Suppress("DEPRECATION")
fun android.app.DialogFragment.stopTouchLogging() {
    Logger.inputLoggerManager.touchLogger.stop(this)
}

/**
 * Starts logging touch events in the popup window.
 *
 * @see [TouchLogger.start]
 */
fun PopupWindow.startTouchLogging() {
    Logger.inputLoggerManager.touchLogger.start(this)
}

/**
 * Stops logging touch events in the popup window.
 *
 * @see [TouchLogger.stop]
 */
fun PopupWindow.stopTouchLogging() {
    Logger.inputLoggerManager.touchLogger.stop(this)
}

/**
 * Processes the [motionEvent] to create a touch event. This function has effect only if touch
 * logging is enabled.
 *
 * @see [TouchLogger.processTouchEvent]
 */
fun processTouchEvent(motionEvent: MotionEvent) {
    Logger.inputLoggerManager.touchLogger.processTouchEvent(motionEvent)
}

/**
 * Enables touch logging from the specified [view].
 *
 * @see [TouchLogger.processTouchEvent]
 */
fun enableTouchLogging(view: View) {
    view.setOnTouchListener(KeyboardTouchListener())
}

internal class KeyboardTouchListener : View.OnTouchListener {

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        if (event != null) {
            processTouchEvent(event)
        }

        return false
    }
}
