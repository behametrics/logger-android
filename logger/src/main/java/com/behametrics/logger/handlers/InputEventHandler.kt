package com.behametrics.logger.handlers

import com.behametrics.logger.inputevents.InputEvent

/** Interface for handling (processing) input events. */
interface InputEventHandler {
    /**
     * Handles (processes) the input event. The handler may, for example, serialize the event and
     * write it into a file or send it over a network.
     */
    fun handle(inputEvent: InputEvent)
}
