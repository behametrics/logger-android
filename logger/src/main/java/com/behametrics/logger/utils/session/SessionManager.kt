package com.behametrics.logger.utils.session

import java.util.*

/**
 * Class providing a unique session identifier (session ID) that can be used to distinguish e.g.
 * users or application sessions for the same user.
 *
 * On instantiation, the [sessionId] is initialized to an empty string. You therefore need to call
 * [generateNewSessionId] explicitly when needed.
 *
 * The class also allows listening to changes in [sessionId].
 */
class SessionManager {

    private val registeredListeners = mutableSetOf<SessionEventListener>()

    /** Holds the current session ID. */
    var sessionId: String = ""
        private set

    /**
     * Generates a new session ID assigned to [sessionId] and triggers an event signifying the
     * session has been changed.
     *
     * @see SessionEventListener
     */
    fun generateNewSessionId() {
        val previousSessionId = sessionId

        sessionId = UUID.randomUUID().toString()

        registeredListeners.forEach { it.onSessionChanged(SessionEvent(sessionId, previousSessionId)) }
    }

    /**
     * Registers the specified [listener] to listen to changes in [sessionId].
     *
     * Registering the same [listener] more than once has no effect.
     */
    fun registerListener(listener: SessionEventListener) {
        registeredListeners.add(listener)
    }

    /**
     * Unregisters the specified [listener] from listening to changes in [sessionId], if the
     * listener is present.
     */
    fun unregisterListener(listener: SessionEventListener) {
        registeredListeners.remove(listener)
    }
}
