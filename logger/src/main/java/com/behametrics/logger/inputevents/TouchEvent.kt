package com.behametrics.logger.inputevents

import com.behametrics.logger.inputloggers.TouchLogger
import com.google.gson.JsonObject

/**
 * Class describing a touch event.
 *
 * @param sessionId       session ID
 * @param timestamp       time of measurement in nanoseconds
 * @param eventType       touch event type
 * @param eventTypeDetail more detailed description of [eventType]
 * @param pointerId       touch pointer ID
 * @param x               touch x-coordinate for the given [pointerId]
 * @param y               touch y-coordinate for the given [pointerId]
 * @param pressure        touch pressure for the given [pointerId]
 * @param size            touch size for the given [pointerId]
 * @param touchMajor      length of the major axis of the touch area for the given [pointerId]
 * @param touchMinor      length of the major axis of the touch area for the given [pointerId]
 * @param rawX            raw touch x-coordinate for the default touch pointer ID
 * @param rawY            raw touch y-coordinate for the default touch pointer ID
 */
class TouchEvent(
    sessionId: String,
    timestamp: Long,
    private val eventType: String,
    private val eventTypeDetail: String,
    private val pointerId: Int,
    private val x: Float,
    private val y: Float,
    private val pressure: Float,
    private val size: Float,
    private val touchMajor: Float,
    private val touchMinor: Float,
    private val rawX: Float,
    private val rawY: Float
) : InputEvent(TouchLogger.INPUT_NAME, sessionId, timestamp) {

    override fun toString(separator: String): String {
        return arrayOf(
            inputName,
            sessionId,
            timestamp,
            eventType,
            eventTypeDetail,
            pointerId,
            x,
            y,
            pressure,
            size,
            touchMajor,
            touchMinor,
            rawX,
            rawY
        ).joinToString(separator)
    }

    override fun getContentFieldNames(): Array<String> {
        return arrayOf(
            "event_type",
            "event_type_detail",
            "pointer_id",
            "x",
            "y",
            "pressure",
            "size",
            "touch_major",
            "touch_minor",
            "raw_x",
            "raw_y"
        )
    }

    override fun fillJsonContent(content: JsonObject) {
        content.addProperty("event_type", eventType)
        content.addProperty("event_type_detail", eventTypeDetail)
        content.addProperty("pointer_id", pointerId)
        content.addProperty("x", x)
        content.addProperty("y", y)
        content.addProperty("pressure", pressure)
        content.addProperty("size", size)
        content.addProperty("touch_major", touchMajor)
        content.addProperty("touch_minor", touchMinor)
        content.addProperty("raw_x", rawX)
        content.addProperty("raw_y", rawY)
    }
}
