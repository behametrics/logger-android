@file:JvmName("KeystrokeLogging")

package com.behametrics.logger.utils.logging

import com.behametrics.logger.Logger
import com.behametrics.logger.inputloggers.KeyboardLogger

/**
 * Logs a key press event. This function has effect only if keystroke logging is enabled.
 *
 * @see [KeyboardLogger.log]
 */
fun logKeyPress(primaryCode: Int, keyName: String? = null) {
    Logger.inputLoggerManager.keyboardLogger.log("press", primaryCode, keyName)
}

/**
 * Logs a key release event. This function has effect only if keystroke logging is enabled.
 *
 * @see [KeyboardLogger.log]
 */
fun logKeyRelease(primaryCode: Int, keyName: String? = null) {
    Logger.inputLoggerManager.keyboardLogger.log("release", primaryCode, keyName)
}
