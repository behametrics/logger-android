package com.behametrics.logger.inputevents

import com.google.gson.Gson
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

// NOTE: Gson assumes floats by default for all numeric types. Therefore, expected integers (such as
// timestamps) are expressed as floats. A custom deserializer for testing purposes is not
// implemented yet due to laziness.

internal class OrientationEventTest {

    private lateinit var orientationEvent: OrientationEvent

    private val gson = Gson()

    @Test
    fun toJson() {
        orientationEvent = OrientationEvent("123-456", 100, "portrait")

        val expectedOutput = mapOf(
            "input" to "orientation",
            "session_id" to "123-456",
            "timestamp" to 100.0,
            "content" to mapOf(
                "orientation" to "portrait"
            )
        )

        Assertions.assertEquals(expectedOutput, gson.fromJson(orientationEvent.toJson(), Map::class.java))
    }
}
