package com.behametrics.logger.inputloggers

import android.content.ComponentCallbacks
import android.content.Context
import android.content.res.Configuration
import com.behametrics.logger.handlers.InputEventDispatcher
import com.behametrics.logger.inputevents.OrientationEvent
import com.behametrics.logger.utils.session.SessionManager
import com.behametrics.logger.utils.getNonSensorEventTimestampNanos
import com.behametrics.logger.utils.maps.DeviceOrientationMap
import com.behametrics.logger.utils.session.SessionEvent

/**
 * Class providing logging of events describing changes in device orientation (portrait, landscape).
 *
 * The orientation is also logged when starting the logger or when the session ID is changed via
 * [sessionManager].
 *
 * @see OrientationEvent
 */
class OrientationLogger(
    inputEventDispatcher: InputEventDispatcher,
    sessionManager: SessionManager
) : InputLogger(inputEventDispatcher, sessionManager, Scopes.DEVICE), ComponentCallbacks {

    private var previousOrientation = -1

    companion object {
        private val deviceOrientationMap = DeviceOrientationMap()

        const val INPUT_NAME = "orientation"
    }

    override fun startLogging(context: Context) {
        logInitialOrientation(context.applicationContext)
        context.applicationContext.registerComponentCallbacks(this)
    }

    override fun stopLogging(context: Context) {
        context.applicationContext.unregisterComponentCallbacks(this)
    }

    override fun onConfigurationChanged(newConfiguration: Configuration) {
        if (newConfiguration.orientation != previousOrientation) {
            log(sessionManager.sessionId, newConfiguration.orientation)

            previousOrientation = newConfiguration.orientation
        }
    }

    override fun onLowMemory() {
    }

    override fun onSessionChanged(event: SessionEvent) {
        log(event.currentSessionId, previousOrientation)
    }

    private fun logInitialOrientation(context: Context) {
        onConfigurationChanged(context.resources.configuration)
    }

    private fun log(sessionId: String, orientation: Int) {
        inputEventDispatcher.dispatch(
            OrientationEvent(
                sessionId,
                getNonSensorEventTimestampNanos(),
                deviceOrientationMap.getName(orientation) ?: ""
            )
        )
    }
}
