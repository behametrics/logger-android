package com.behametrics.logger.utils.session

import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class SessionManagerTest {

    private lateinit var sessionManager: SessionManager
    private lateinit var sessionEventListenerTestImpl: SessionEventListenerTestImpl

    @BeforeEach
    fun setUp() {
        sessionManager = SessionManager()
        sessionEventListenerTestImpl =
            SessionEventListenerTestImpl()
    }

    @Test
    fun registerListener_ChangingSessionTriggersEvent() {
        sessionManager.generateNewSessionId()

        val expectedPreviousSessionId = sessionManager.sessionId

        sessionManager.registerListener(sessionEventListenerTestImpl)
        sessionManager.generateNewSessionId()

        assertEquals(sessionManager.sessionId, sessionEventListenerTestImpl.currentSessionId)
        assertEquals(expectedPreviousSessionId, sessionEventListenerTestImpl.previousSessionId)
    }

    @Test
    fun registerListener_SubsequentCallsToRegisterHaveNoEffect() {
        sessionEventListenerTestImpl = spyk(SessionEventListenerTestImpl())

        sessionManager.registerListener(sessionEventListenerTestImpl)
        sessionManager.registerListener(sessionEventListenerTestImpl)
        sessionManager.generateNewSessionId()

        verify (exactly = 1) { sessionEventListenerTestImpl.onSessionChanged(any()) }
    }

    @Test
    fun registerListener_RegisterMultipleListeners() {
        sessionManager.generateNewSessionId()

        val expectedPreviousSessionId = sessionManager.sessionId
        val secondSessionEventListenerTestImpl =
            SessionEventListenerTestImpl()

        sessionManager.registerListener(sessionEventListenerTestImpl)
        sessionManager.registerListener(secondSessionEventListenerTestImpl)
        sessionManager.generateNewSessionId()

        assertEquals(sessionManager.sessionId, sessionEventListenerTestImpl.currentSessionId)
        assertEquals(expectedPreviousSessionId, sessionEventListenerTestImpl.previousSessionId)
        assertEquals(sessionManager.sessionId, secondSessionEventListenerTestImpl.currentSessionId)
        assertEquals(expectedPreviousSessionId, secondSessionEventListenerTestImpl.previousSessionId)
    }

    @Test
    fun unregisterListener_UnregisteredManagerYieldsNoEvent() {
        sessionManager.registerListener(sessionEventListenerTestImpl)
        sessionManager.unregisterListener(sessionEventListenerTestImpl)
        sessionManager.generateNewSessionId()

        assertEquals("", sessionEventListenerTestImpl.currentSessionId)
        assertEquals("", sessionEventListenerTestImpl.previousSessionId)
    }

    internal class SessionEventListenerTestImpl : SessionEventListener {

        var currentSessionId = ""
            private set

        var previousSessionId = ""
            private set

        override fun onSessionChanged(event: SessionEvent) {
            currentSessionId = event.currentSessionId
            previousSessionId = event.previousSessionId
        }
    }
}
